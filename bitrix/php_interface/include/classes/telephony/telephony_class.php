<?
class CTelephony{

    const REQUISITE_PRESET_ID = '9';

    public static $access_group_list = [1, 2, 3];
    public static $url_bus = 're-store.by/b24_exchange/';
    public static $scheme_http = 'https://';

    function __construct(){
        $this->init_obj();
    }

    function init_obj(){
        Bitrix\Main\Loader::includeModule('pull');
    }

    function user_subscribe($user_id){
        CPullWatch::Add($user_id, 'telephony_' . $user_id);
    }

    public static function get_bonus_cart($user_id){
        $ar = [
            'bonus_cart' => false,
            'bonus_points' => false
        ];
        $url = self::$scheme_http . self::$url_bus;
        $ch = curl_init();

        $send_data = [
            'action' => 'get_bonus_cart',
            'user_id' => $user_id,
            'remote_ip' => $_SERVER['REMOTE_ADDR']
        ];

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_USERPWD => 'test:test',
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_POSTFIELDS => $send_data
        );
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $curl_info = curl_getinfo($ch);
        $response = json_decode($response);

        if(isset($response->bonus_cart) && !empty($response->bonus_cart)){
            $ar['bonus_cart'] = $response->bonus_cart;
        }

        if(isset($response->bonus_points) && !empty($response->bonus_points)){
            $ar['bonus_points'] = $response->bonus_points;
        }

        return $ar;
    }

    public static function get_user_data_magento($send_data){
	    $client = new SoapClient('https://new.doordesignlab.com/api/soap/?wsdl');

	    $session = $client->login('admin', '35914sa');
	
	    $result = $client->call($session, 'customer.list');
	    var_dump ($result);
		$client->endSession($session);
		   
	    return $result;
    }
    
    public static function get_user_data($send_data){

        $url = self::$scheme_http . self::$url_bus;
        $ch = curl_init();

        $send_data['action'] = 'get_user_data';
        $send_data['remote_ip'] = $_SERVER['REMOTE_ADDR'];

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_USERPWD => 'test:test',
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_POSTFIELDS => $send_data
        );
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $curl_info = curl_getinfo($ch);
        $response = json_encode($response);

        return $response;
    }

    public static function register_user_bus($send_data){

        $url = self::$scheme_http . self::$url_bus;
        $ch = curl_init();

        $send_data['action'] = 'register_user_bus';
        $send_data['remote_ip'] = $_SERVER['REMOTE_ADDR'];

        if(!empty($send_data['phone'])){
            $send_data['email'] = $send_data['phone'] . '@restoracia.by';
        }

        if(!empty($send_data['user_name'])){
            //$send_data['user_last_name'] = $send_data['user_name'];
        }

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_USERPWD => 'test:test',
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_POSTFIELDS => $send_data
        );
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $curl_info = curl_getinfo($ch);
        $ar_response = json_decode($response, 1);

        if((int)$ar_response['user_id'] > 0){
            $ar_response['contact_id'] = self::create_crm_contact($ar_response['user_data']);
        }

        return json_encode($ar_response);
    }

    public static function create_crm_contact($data){
        CModule::IncludeModule("crm");
        $obj = new CAllCrmContact();

        $prop_uf_user_id = CAllUserTypeEntity::GetList(
            ['ID' => 'ASC'],
            [
                'XML_ID' => 'BUS_USER_ID',
                'ENTITY_ID' => 'CRM_CONTACT',
                'CHECK_PERMISSIONS' => 'N'
            ]
        )->Fetch();

        $origin_id = "{$data['ID']}#{$data['EMAIL']}#{$data['LAST_NAME']} {$data['NAME']} {$data['SECOND_NAME']}";

        if(!empty($data['NAME']) && $data['NAME'] != ''){
            $origin_id .= '|' . $data['NAME'];
        };

        if(!empty($data['LAST_NAME']) && $data['LAST_NAME'] != ''){
            $origin_id .= '|' . $data['LAST_NAME'];
        };

        $add_fields = [
            'NAME' => $data['NAME'],
            'LAST_NAME' => $data['LAST_NAME'],
            'ORIGIN_ID' => $origin_id,
            'SOURCE_ID' => 'WEB',
            'OPENED' => 'Y',
            'TYPE_ID' => 'CLIENT',
            'ORIGINATOR_ID' => 9, // номер сайта импортера
            'FM' => array(
                //'EMAIL' => array(
                //    'n0' => array('VALUE' => $data['EMAIL'], 'VALUE_TYPE' => 'WORK')
                //),
                'PHONE' => array(
                    'n0' => array('VALUE' => $data['PERSONAL_PHONE'], 'VALUE_TYPE' => 'WORK')
                )
            ),
        ];
        if(!empty($prop_uf_user_id)){
            $add_fields[$prop_uf_user_id['FIELD_NAME']] = $data['ID'];
        }
        return $obj->Add($add_fields);
    }

    public static function user_push($params_event){

        global $USER;
        //if($USER->IsAuthorized()){
        if((int)$params_event['fields']['USER_ID'] > 0){
            //$ar_user_groups = (array)CUser::GetUserGroup($USER->GetID());
            $ar_user_groups = (array)CUser::GetUserGroup($params_event['fields']['USER_ID']);
            $result = array_intersect($ar_user_groups, (array)CTelephony::$access_group_list);
            if(!empty($result)){
                CPullWatch::AddToStack('telephony_' . $params_event['fields']['USER_ID'],
                    Array(
                        'module_id' => 'telephony',
                        'command' => 'message',
                        'params' => $params_event
                    )
                );
            }
        }
    }

    public static function call_table_after_upd($event){

        $params_event = $event->getParameters();
        $params = [
            'ENTITY_TYPE' => $params_event['fields']['CRM_ENTITY_TYPE'],
            'ENTITY_ID' => $params_event['fields']['CRM_ENTITY_ID']
        ];

        $params_event['ENTITY_DATA'] = CTelephony::get_entity_data($params);
        //CTelephony::user_push($params_event);
    }

    public static function act_table_after_add($fields){
        if((int)$fields['OWNER_ID'] > 0 && (int)$fields['OWNER_TYPE_ID'] > 0){
            $entity = '';
            switch ($fields['OWNER_TYPE_ID']){
//                case 1:
//                    $entity = 'LEAD';
//                    break;
//                case 2:
//                    $entity = 'DEAL';
//                    break;
                case 3:
                    $arFilter = array(
                        'ID' => $fields['OWNER_ID'],
                        'CHECK_PERMISSIONS' => 'N'
                    );
                    $arSelect = array(
                        'ID',
                        'COMPANY_ID',
                    );

                    $db_contact = CAllCrmContact::GetList(array(), $arFilter, $arSelect);
                    if($arContactFields = $db_contact->Fetch())
                    {
                        $companyId = intval($arContactFields['COMPANY_ID']);
                        if($companyId > 0)
                        {
                            $entity = 'COMPANY';
                            $fields['OWNER_ID'] = $arContactFields['COMPANY_ID'];
                            $fields['OWNER_TYPE_ID'] = CCrmOwnerType::Company;
                        }
                        else
                        {
                            $entity = 'CONTACT';
                        }
                    }
                    break;
                case 4:
                    $entity = 'COMPANY';
                    break;
            }

            if(empty($entity) || ($entity === 'CONTACT')){
                return;
            }

            $params = [
                'ENTITY_TYPE' => $entity,
                'ENTITY_ID' => $fields['OWNER_ID'],
                'CALLER_ID' => $fields["CALL_FIELDS"]['CALLER_ID'],
            ];
            $params_event = [
                'fields' => [
                    'CRM_ENTITY_TYPE' => $entity,
                    'CRM_ENTITY_ID' => $fields['OWNER_ID'],
                    'USER_ID' => $fields['RESPONSIBLE_ID']
                ],
                'ENTITY_DATA' => CTelephony::get_entity_data($params)
            ];
            if(is_array($fields['COMMUNICATIONS']) && count($fields['COMMUNICATIONS']) > 0){
                foreach ($fields['COMMUNICATIONS'] as $ar){
                    if($ar['TYPE'] == 'PHONE' && !empty($ar['VALUE'])){
                        $params_event['fields']['CALLER_ID'] = $ar['VALUE'];
                        break;
                    }
                }
            }

            CTelephony::user_push($params_event);
        }
    }

    public static function act_table_after_upd($id){

        if((int)$id > 0){
            $ar_act = CAllCrmActivity::GetByID($id);
            if(!empty($ar_act)){
                self::act_table_after_add($id,$ar_act);
            }
        }
    }

    public static function recall_card($params){

        $response = ['status' => 'error', 'timestamp' => time(true)];
        $has_error = false;
        $ar_user = false;

        if(CModule::IncludeModule("crm") && CModule::IncludeModule("voximplant")){

            if(empty($params['call_id'])){
                $response['error'] = 'empty call id';
                $has_error = true;
            }

            if(empty($params['user_id'])){

                if(!empty($params['phone_inner'])){
                    $ar_user = \CUser::GetList($by='ID',$order='ASC',['=UF_PHONE_INNER' => $params['phone_inner']])->Fetch();
                }

                if(empty($ar_user)){
                    $response['error'] = 'empty user data';
                    $has_error = true;
                }else{
                    $params['user_id'] = $ar_user['ID'];
                }
            }else{

                if(is_numeric($params['user_id'])){
                    $ar_user = \CUser::GetList($by='ID',$order='ASC',['ID' => $params['user_id']])->Fetch();
                }

                if(empty($ar_user)){
                    $response['error'] = 'wrong user id';
                    $params['user_id'] = false;
                    $has_error = true;
                }else{
                    $params['user_id'] = $ar_user['ID'];
                }

            }


            if(!$has_error){
                $res = Bitrix\Voximplant\Model\CallTable::getList(Array(
                    'select'=>Array('*'),
                    'filter'=>Array(
                        '=CALL_ID' => (string)$params['call_id']
                    )
                ));

                $call = $res->fetch();

                if(empty($call)){
                    $response['error'] = 'empty call data (error call id)';
                    $has_error = true;
                }

                if(!is_array($call['CRM_BINDINGS']) || !count($call['CRM_BINDINGS']) || !$call['CRM_BINDINGS'][0]['OWNER_ID'])
                {
                    $response['error'] = 'empty entity id';
                    $has_error = true;
                }
                else
                {
                    $arOwnerFields = array(
                        'RESPONSIBLE_ID' => $params['user_id'],
                        'OWNER_ID' => '',
                        'OWNER_TYPE_ID' => '',
                        'CALL_FIELDS' => $call,
                    );
                    foreach($call['CRM_BINDINGS'] as $owner)
                    {
                        if($owner['OWNER_TYPE_ID'] > intval($arOwnerFields['OWNER_TYPE_ID']))
                        {
                            $arOwnerFields['OWNER_ID'] = $owner['OWNER_ID'];
                            $arOwnerFields['OWNER_TYPE_ID'] = $owner['OWNER_TYPE_ID'];
                        }
                    }
                    self::act_table_after_add($arOwnerFields);
                    $response['status'] = 'ok';
                }
            }
        }

        return $response;
    }

    public static function get_entity_data($params){
        $ar_result['LEAD'] = [];
        $ar_result['CONTACT'] = [];
        $ar_result['COMPANY'] = [];
        switch ($params['ENTITY_TYPE']){
            case 'CONTACT':
                if((int)$params['ENTITY_ID'] > 0){

                    $filter = [
                        'ID' => $params['ENTITY_ID'],
                        'CHECK_PERMISSIONS' => 'N'
                    ];
                    $db_contact = CAllCrmContact::GetList([],$filter);
                    if($ar_contact = $db_contact->Fetch()){
                        $prop_uf_user_id = CAllUserTypeEntity::GetList(
                            ['ID' => 'ASC'],
                            ['XML_ID' => 'BUS_USER_ID', 'ENTITY_ID' => 'CRM_CONTACT', 'CHECK_PERMISSIONS' => 'N']
                        )->Fetch();
                        // определяем пользователя в БУС
                        $ar_result['BUS_USER_ID'] = $ar_contact['BUS_USER_ID'] = $ar_contact[$prop_uf_user_id['FIELD_NAME']];

                        $db_crm_field = CCrmFieldMulti::GetList(
                            ['ID' => 'asc'],
                            ['ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $ar_contact['ID']]
                        );
                        while ($ar_crm_field = $db_crm_field->Fetch()){
                            $ar_company[$ar_crm_field['TYPE_ID']][] = $ar_crm_field;
//                            if($ar_crm_field['TYPE_ID'] == 'PHONE'){
//                                $ar_result['PHONE'] = $ar_crm_field['VALUE'];
//                            }
                        }
                    }
                    $ar_result['PHONE'] = $params['CALLER_ID'];
                    $ar_result['CONTACT'] = $ar_contact;
                }
                break;
            case 'LEAD':
                if((int)$params['ENTITY_ID'] > 0){

                    $ar_lead = [];
                    $filter = [
                        'ID' => $params['ENTITY_ID'],
                        'CHECK_PERMISSIONS' => 'N'
                    ];
                    $db_lead = CAllCrmLead::GetList([],$filter);
                    if ($ar_lead = $db_lead->Fetch()){

                        $prop_uf_user_id = CAllUserTypeEntity::GetList(
                            ['ID' => 'ASC'],
                            ['XML_ID' => 'BUS_USER_ID', 'ENTITY_ID' => 'CRM_LEAD']
                        )->Fetch();
                        // определяем пользователя в БУС
                        $ar_result['BUS_USER_ID'] = $ar_lead['BUS_USER_ID'] = $ar_lead[$prop_uf_user_id['FIELD_NAME']];


                        $db_crm_field = CCrmFieldMulti::GetList(
                            ['ID' => 'asc'],
                            ['ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $ar_lead['ID']]
                        );
                        while ($ar_crm_field = $db_crm_field->Fetch()){
                            $ar_lead[$ar_crm_field['TYPE_ID']] = $ar_crm_field['VALUE'];
                            if($ar_crm_field['TYPE_ID'] == 'PHONE'){
                                $ar_result['PHONE'] = $ar_crm_field['VALUE'];
                            }
                        }
                    }
                    $ar_result['LEAD'] = $ar_lead;
                }
                break;
            case 'COMPANY':
                if((int)$params['ENTITY_ID'] > 0){

                    $filter = [
                        'ID' => $params['ENTITY_ID'],
                        'CHECK_PERMISSIONS' => 'N'
                    ];
                    $db_company = CAllCrmCompany::GetList([],$filter);
                    if ($ar_company = $db_company->Fetch()){

                        $prop_uf_user_id = CAllUserTypeEntity::GetList(
                            ['ID' => 'ASC'],
                            ['XML_ID' => 'BUS_USER_ID', 'ENTITY_ID' => 'CRM_COMPANY', 'CHECK_PERMISSIONS' => 'N']
                        )->Fetch();
                        // определяем пользователя в БУС
                        $ar_result['BUS_USER_ID'] = $ar_company['BUS_USER_ID'] = $ar_company[$prop_uf_user_id['FIELD_NAME']];


                        $db_crm_field = CCrmFieldMulti::GetList(
                            ['ID' => 'asc'],
                            ['ENTITY_ID' => 'COMPANY', 'ELEMENT_ID' => $ar_company['ID']]
                        );
                        while ($ar_crm_field = $db_crm_field->Fetch()){
                            $ar_company[$ar_crm_field['TYPE_ID']][] = $ar_crm_field;
//                            if($ar_crm_field['TYPE_ID'] == 'PHONE'){
//                                $ar_result['PHONE'] = $ar_crm_field['VALUE'];
//                            }
                        }
                    }

                    $ar_result['PHONE'] = $params['CALLER_ID'];

                    $obRequisite = new \Bitrix\Crm\EntityRequisite();
                    $arFilter = array(
                        "filter" => array(
                            "ENTITY_ID" => $params['ENTITY_ID'],
                            "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                            "PRESET_ID" => self::REQUISITE_PRESET_ID,
                        ),
                    );
                    $res = $obRequisite->getList($arFilter);

                    $ar_company['REQUISITE_NAMES'] = $obRequisite->getUserFieldsTitles();
                    if($arFields = $res->Fetch())
                    {
                        $ar_company['REQUISITE'][] = $arFields;
                    }

                    $ar_result['COMPANY'] = $ar_company;
                }
                break;
        }

        if((int)$ar_result['BUS_USER_ID'] > 0){
            $ar_bonus = CTelephony::get_bonus_cart($ar_result['BUS_USER_ID']);
            $ar_result['BONUS_CART'] = $ar_bonus['bonus_cart'];
            $ar_result['BONUS_POINTS'] = $ar_bonus['bonus_points'];
        }

        return $ar_result;
    }

    public static function get_contact_deal_statistic($contact_id){
        $response = [];
        if(empty($contact_id) || (int)$contact_id <= 0){
            return json_encode($response);
        }

        CModule::IncludeModule("crm");

        $entity_id = 'DEAL_STAGE';
        $db_status = CCrmStatus::GetList(['SORT'=>'ASC'],['ENTITY_ID' => $entity_id]);

        while ($ar_status = $db_status->Fetch()){
            $ar_status['PRICE'] = 0;
            $ar_status['DEAL_LIST'] = [];
            $response[$ar_status['STATUS_ID']] = $ar_status;
        }

        if(count($response) > 0){
            $filter = ['CONTACT_ID' => $contact_id, 'CHECK_PERMISSIONS' => 'N'];
            $db_deal = CAllCrmDeal::GetList( ['ID'=>'ASC'], $filter);
            while ($ar_deal = $db_deal->Fetch()){
                if(isset($response[$ar_deal['STAGE_ID']])){
                    $response[$ar_deal['STAGE_ID']]['DEAL_LIST'][$ar_deal['ID']] = $ar_deal;
                    $response[$ar_deal['STAGE_ID']]['PRICE'] += $ar_deal['OPPORTUNITY'];
                }
            }
        }

        return json_encode($response);
    }
}