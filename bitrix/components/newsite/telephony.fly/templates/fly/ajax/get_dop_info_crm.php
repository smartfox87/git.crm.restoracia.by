<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule("crm");
function getUserCRMinfo($filter=false){
	if(!$filter)
		return;
	
        $db_company = CAllCrmCompany::GetList(array(),$filter);
        if($ar_company = $db_company->Fetch()){
			$debit = explode('|',$ar_company['UF_CRM_1550236593']);
			$areras = explode('|',$ar_company['UF_CRM_1550236636']);
			if($ar_company['UF_CRM_1550229507'] == 58){
				$shipment_status = 'Разрешена';
			}
			else if($ar_company['UF_CRM_1550229507'] == 59)
                            {
                                $shipment_status = '<span style="color:red;">Запрещена</span>';
                            }
			         			
			
$res = array('UF_DEBT'=>'<br>Общая задолженность: '.number_format($debit[0], 0, ',', ' ').' BYN','UF_ARREARS'=>'<br>Просроченная: '.number_format($areras[0], 0, ',', ' ').' BYN','NAME'=>'<a target="_blank" onclick="close_tab(); return true;" href="/crm/company/details/'.$ar_company['ID'].'/">'.$ar_company['TITLE'].'</a>','ID'=>'<br>(компания '.$ar_company['ID'].')', 'STATUS'=>'<br/>Статус отгрузки: '.$shipment_status);
}
return $res;
}

if(!empty($_REQUEST['INN'])){
	
	$req = new \Bitrix\Crm\EntityRequisite();
$rs = $req->getList([
  "filter" => [
     "ENTITY_TYPE_ID" => 4, 
    'PRESET_ID' => 9,
   'RQ_INN'=>intval($_REQUEST['INN'])
  ]
]);
$row = $rs->fetch();

	
	
	$result = getUserCRMinfo(array('ID' => $row['ENTITY_ID']));
	
	echo json_encode($result);
	
}
else{
	echo json_encode('no');
}