<?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $USER;
$result['lead'] = [];

if (!CModule::IncludeModule("crm") || !CModule::IncludeModule("iblock") || !CModule::IncludeModule("socialnetwork")) {
    echo json_encode($result);
}

$id = false;
$crm_lead = new CCrmLead();

$fields = [
    'TITLE' => $_REQUEST['name'],
    'NAME' => $_REQUEST['name'],
    'LAST_NAME' => '',
    'ADDRESS' => '',
    'COMMENTS' => trim($_REQUEST['comment']),
    'ASSIGNED_BY_ID' => $USER->GetID(),
    'FM' => ['PHONE' => ['n0' => ['VALUE' => $_REQUEST['phone'], 'VALUE_TYPE' => 'WORK'] ] ]
];

if($_REQUEST['action'] == 'create' && !isset($_REQUEST['lead_id'])){

    $id = $crm_lead->Add($fields, true);
    $result['action'] = 'create';

}elseif ($_REQUEST['action'] == 'create' && !empty($_REQUEST['lead_id'])){
    $id = (int)$_REQUEST['lead_id'];
    $crm_lead->Update($id, $fields);
    $result['action'] = 'update';
}

if((int)$id > 0){
    $result['lead'] = CCrmLead::GetList(['ID'=>'DESC'],['ID'=>$id])->Fetch();
}

echo json_encode($result);




