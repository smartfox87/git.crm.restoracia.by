<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$result['reload'] = false;
$result['contact'] = false;
$result['company'] = false;
$result['lead'] = false;
$result['success'] = [
	'lead' => [],
	'company' => [],
	'contact' => []
];

if (!CModule::IncludeModule("crm") || !CModule::IncludeModule("iblock") || !CModule::IncludeModule("socialnetwork")) {
	echo json_encode($result);
}

if($_REQUEST['action'] == 'get'){

	// получение данных по лиду
	if(!empty($_REQUEST['lead_id'])){
		$ar_leads = [];
		$filter = ['ID' => $_REQUEST['lead_id']];
		$db_lead = CAllCrmLead::GetList([],$filter);
		if ($ar_lead = $db_lead->Fetch()){

            $prop_uf_user_id = CAllUserTypeEntity::GetList(
                ['ID' => 'ASC'],
                ['XML_ID' => 'BUS_USER_ID', 'ENTITY_ID' => 'CRM_LEAD']
            )->Fetch();
            // определяем пользователя в БУС
            $ar_lead['BUS_USER_ID'] = $ar_lead[$prop_uf_user_id['FIELD_NAME']];


            $prop_uf_product_interest = CAllUserTypeEntity::GetList(
                ['ID' => 'ASC'],
                ['XML_ID' => 'PRODUCT_INTEREST', 'ENTITY_ID' => 'CRM_LEAD']
            )->Fetch();
            if(!empty($prop_uf_product_interest) && $prop_uf_product_interest['USER_TYPE_ID'] == 'enumeration'){
                $ob_enum = new CUserFieldEnum;
                $filter = ["USER_FIELD_ID" => $prop_uf_product_interest['ID']];
                $rs_enum = $ob_enum->GetList([], $filter);
                $ar_lead[$prop_uf_product_interest['FIELD_NAME']] = is_array($ar_lead[$prop_uf_product_interest['FIELD_NAME']]) ? $ar_lead[$prop_uf_product_interest['FIELD_NAME']] : [$ar_lead[$prop_uf_product_interest['FIELD_NAME']]];
                while($ar_enum = $rs_enum->GetNext()){
                    $ar_lead['PRODUCT_INTEREST'][$ar_enum['ID']] = [
                        'name' => $ar_enum['VALUE'],
                        'id' => $ar_enum['ID'],
                        'field_name' => $prop_uf_product_interest['FIELD_NAME'],
                        'selected' => in_array($ar_enum['ID'], $ar_lead[$prop_uf_product_interest['FIELD_NAME']]),
                    ];
                }
            }
		}
		$result['lead'] = $ar_lead;
	}

    if(!empty($_REQUEST['contact_id'])){
        $ar_contact = [];
        $filter = ['ID' => $_REQUEST['contact_id']];
        $db_contact = CAllCrmContact::GetList([],$filter);
        if($ar_contact = $db_contact->Fetch()){
            $prop_uf_user_id = CAllUserTypeEntity::GetList(
                ['ID' => 'ASC'],
                ['XML_ID' => 'BUS_USER_ID', 'ENTITY_ID' => 'CRM_CONTACT']
            )->Fetch();
            // определяем пользователя в БУС
            $ar_contact['BUS_USER_ID'] = $ar_contact[$prop_uf_user_id['FIELD_NAME']];


            $prop_uf_product_interest = CAllUserTypeEntity::GetList(
                ['ID' => 'ASC'],
                ['XML_ID' => 'PRODUCT_INTEREST', 'ENTITY_ID' => 'CRM_CONTACT']
            )->Fetch();
            if(!empty($prop_uf_product_interest) && $prop_uf_product_interest['USER_TYPE_ID'] == 'enumeration'){
                $ob_enum = new CUserFieldEnum;
                $filter = ["USER_FIELD_ID" => $prop_uf_product_interest['ID']];
                $rs_enum = $ob_enum->GetList([], $filter);
                $ar_contact[$prop_uf_product_interest['FIELD_NAME']] = is_array($ar_contact[$prop_uf_product_interest['FIELD_NAME']]) ? $ar_contact[$prop_uf_product_interest['FIELD_NAME']] : [$ar_contact[$prop_uf_product_interest['FIELD_NAME']]];
                while($ar_enum = $rs_enum->GetNext()){
                    $ar_contact['PRODUCT_INTEREST'][$ar_enum['ID']] = [
                        'name' => $ar_enum['VALUE'],
                        'id' => $ar_enum['ID'],
                        'field_name' => $prop_uf_product_interest['FIELD_NAME'],
                        'selected' => in_array($ar_enum['ID'], $ar_contact[$prop_uf_product_interest['FIELD_NAME']]),
                    ];
                }
            }
        }
        $result['contact'] = $ar_contact;
    }

    if(!empty($_REQUEST['company_id'])){
        $ar_company = [];
        $filter = ['ID' => $_REQUEST['company_id']];
        $db_company = CAllCrmCompany::GetList([],$filter);
        if($ar_company = $db_company->Fetch()){
            $prop_uf_user_id = CAllUserTypeEntity::GetList(
                ['ID' => 'ASC'],
                ['XML_ID' => 'BUS_USER_ID', 'ENTITY_ID' => 'CRM_COMPANY']
            )->Fetch();
            // определяем пользователя в БУС
            $ar_company['BUS_USER_ID'] = $ar_company[$prop_uf_user_id['FIELD_NAME']];


            $prop_uf_product_interest = CAllUserTypeEntity::GetList(
                ['ID' => 'ASC'],
                ['XML_ID' => 'PRODUCT_INTEREST', 'ENTITY_ID' => 'CRM_COMPANY']
            )->Fetch();
            if(!empty($prop_uf_product_interest) && $prop_uf_product_interest['USER_TYPE_ID'] == 'enumeration'){
                $ob_enum = new CUserFieldEnum;
                $filter = ["USER_FIELD_ID" => $prop_uf_product_interest['ID']];
                $rs_enum = $ob_enum->GetList([], $filter);
                $ar_company[$prop_uf_product_interest['FIELD_NAME']] = is_array($ar_company[$prop_uf_product_interest['FIELD_NAME']]) ? $ar_company[$prop_uf_product_interest['FIELD_NAME']] : [$ar_company[$prop_uf_product_interest['FIELD_NAME']]];
                while($ar_enum = $rs_enum->GetNext()){
                    $ar_company['PRODUCT_INTEREST'][$ar_enum['ID']] = [
                        'name' => $ar_enum['VALUE'],
                        'id' => $ar_enum['ID'],
                        'field_name' => $prop_uf_product_interest['FIELD_NAME'],
                        'selected' => in_array($ar_enum['ID'], $ar_company[$prop_uf_product_interest['FIELD_NAME']]),
                    ];
                }
            }
        }
        $result['company'] = $ar_company;
    }

}elseif ($_REQUEST['action'] == 'update' && !empty($_REQUEST['entity']) && (int)$_REQUEST['entity_id'] > 0 ){

    // обновление списка интересов
    if(!empty($_REQUEST['field_name'])){
        $obj = false;
        switch ($_REQUEST['entity']){
            case 'contact':
                $obj = new CAllCrmContact();
                break;
            case 'lead':
                $obj = new CAllCrmLead();
                break;
            case 'company':
                $obj = new CAllCrmCompany();
                break;
        }

        if(!empty($obj)){
            $value = count($_REQUEST[$_REQUEST['field_name']]) == 0 || empty($_REQUEST[$_REQUEST['field_name']]) ? [] : $_REQUEST[$_REQUEST['field_name']];
            $ar_fields_upd = [$_REQUEST['field_name'] => $value];
            $res = $obj->Update( $_REQUEST['entity_id'], $ar_fields_upd);
            if($res){
                $result['success'][$_REQUEST['entity']]['list'] = true;
            }
        }
    }

    // обновление заметок к сущности
	if(!empty($_REQUEST['notes'])){

		$result['notes'] = $_REQUEST['notes'];
        $id_mess = false;
        $ar_add = [];
        switch ($_REQUEST['entity']){
            case 'contact':

                $ar_add = [
                    'CALLBACK_FUNC' => false,
                    'ENABLE_COMMENTS' => 'Y',
                    '=LOG_DATE' => 'now()',
                    'ENTITY_ID' => $_REQUEST['entity_id'],
                    'ENTITY_TYPE' => 'CRMCONTACT',
                    'EVENT_ID' => 'crm_contact_message',
                    'SITE_ID' => [SITE_ID],
                    'MESSAGE' => $result['notes'],
                    'MODULE_ID' => 'crm_shared',
                    'SOURCE_ID' => 'crm_contact_message',
                    'TITLE' => '__EMPTY__',
                    'USER_ID' => $USER->GetID(),
                    'URL' => "/crm/contact/show/{$_REQUEST['entity_id']}/"//$APPLICATION->GetCurPage(),
                ];

                break;
            case 'lead':

                $ar_add = [
                    'CALLBACK_FUNC' => false,
                    'ENABLE_COMMENTS' => 'Y',
                    '=LOG_DATE' => 'now()',
                    'ENTITY_ID' => $_REQUEST['entity_id'],
                    'ENTITY_TYPE' => 'CRMLEAD',
                    'EVENT_ID' => 'crm_lead_message',
                    'SITE_ID' => [SITE_ID],
                    'MESSAGE' => $result['notes'],
                    'MODULE_ID' => 'crm_shared',
                    'SOURCE_ID' => 'crm_lead_message',
                    'TITLE' => '__EMPTY__',
                    'USER_ID' => $USER->GetID(),
                    'URL' => "/crm/lead/show/{$_REQUEST['entity_id']}/"//$APPLICATION->GetCurPage(),
                ];

                break;
            case 'company':

                $ar_add = [
                    'CALLBACK_FUNC' => false,
                    'ENABLE_COMMENTS' => 'Y',
                    '=LOG_DATE' => 'now()',
                    'ENTITY_ID' => $_REQUEST['entity_id'],
                    'ENTITY_TYPE' => 'CRMCOMPANY',
                    'EVENT_ID' => 'crm_company_message',
                    'SITE_ID' => [SITE_ID],
                    'MESSAGE' => $result['notes'],
                    'MODULE_ID' => 'crm_shared',
                    'SOURCE_ID' => 'crm_company_message',
                    'TITLE' => '__EMPTY__',
                    'USER_ID' => $USER->GetID(),
                    'URL' => "/crm/company/show/{$_REQUEST['entity_id']}/"//$APPLICATION->GetCurPage(),
                ];

                break;
        }

        if(!empty($ar_add)){
            $id_mess = CSocNetLog::Add($ar_add);
            if(!$id_mess){
                $result['success'][$_REQUEST['entity']]['notes'] = false;
            }
        }

        if((int)$id_mess > 0){
            $ar_upd = [
                'RATING_ENTITY_ID' => $id_mess,
                'RATING_TYPE_ID' => 'LOG_ENTRY'
            ];
            CSocNetLog::Update($id_mess, $ar_upd);
            $result['success'][$_REQUEST['entity']]['notes'] = true;
            $result['reload'] = true;
        }

	}
}

echo json_encode($result);




