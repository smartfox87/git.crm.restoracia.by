<?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $USER;
$result = [
    'lead' => [],
    'errors' => [],
    'fields' => []
];

if (!CModule::IncludeModule("crm") || !CModule::IncludeModule("iblock") || !CModule::IncludeModule("socialnetwork")) {
    echo json_encode($result);
}

//echo json_encode($_REQUEST); return;

$id = false;
$crm_lead = new CCrmLead();
$comment = trim($_REQUEST['comment']);
$other_reason = trim($_REQUEST['other_reason']);
$name = trim($_REQUEST['name']);

$phone = !empty($_REQUEST['phone']) ? trim(str_replace(' ','',$_REQUEST['phone'])) : false;

$ar_target_field = false;
$no_target = false; // не целевой лид
if(!empty($_REQUEST['target_field'])){
    $ar_target_field = \CAllUserTypeEntity::GetList(
        ['ID' => 'ASC'],
        ['ENTITY_ID' => 'CRM_LEAD','ID' => (int)$_REQUEST['target_field']]
    )->Fetch();
    // устанавливаем цель (целевой или нет)
    if(!empty($ar_target_field['FIELD_NAME'])){
        $fields[$ar_target_field['FIELD_NAME']] = $_REQUEST['target'];
    }

    $db_enum = \CUserFieldEnum::GetList(['SORT' => 'ASC'],['USER_FIELD_ID' => $ar_target_field['ID'] ]);
    while ($ar_enum = $db_enum->Fetch()){
        if($ar_enum['XML_ID'] == 'no' && $_REQUEST['target'] == $ar_enum['ID']){
            $no_target = true;
            break;
        }
    }
}

// обработка поля "другая причина"
$ar_reason_call_field = false;
$ar_reason_call_list = [];
if(!empty($_REQUEST['reasons_call_field'])){
    $db_reason_list = \CUserFieldEnum::GetList(['SORT' => 'ASC'],['USER_FIELD_ID' => (int)$_REQUEST['reasons_call_field'] ]);
    $reason_other_val = false;
    while ($ar_val = $db_reason_list->Fetch()){
        $ar_reason_call_list[$ar_val['ID']] = $ar_val;
        if($ar_val['XML_ID'] == 'other'){
            $reason_other_val = $ar_val;
        }
    }

    if(
        !empty($reason_other_val)
        && in_array($reason_other_val['ID'], $_REQUEST['reasons_list'])
        && empty($other_reason)
        && $no_target
    ){
        $result['errors']['other_reason'] = true;
    }

    // проброс значения "другая причина"
    $ar_reason_call_field = \CAllUserTypeEntity::GetList(
        ['ID' => 'ASC'],
        ['ENTITY_ID' => 'CRM_LEAD','ID' => (int)$_REQUEST['reasons_call_field']]
    )->Fetch();
    if(!empty($ar_reason_call_field['FIELD_NAME'])){
        $fields[$ar_reason_call_field['FIELD_NAME']] = $_REQUEST['reasons_list'];
    }

}


$ar_other_reasons_field = false;
$other_reason = '';
if(!empty($_REQUEST['other_reasons_field'])){
    $ar_other_reasons_field = \CAllUserTypeEntity::GetList(
        ['ID' => 'ASC'],
        ['ENTITY_ID' => 'CRM_LEAD','ID' => (int)$_REQUEST['other_reasons_field']]
    )->Fetch();

    if(in_array($reason_other_val['ID'], $_REQUEST['reasons_list'])){
        $other_reason = $_REQUEST['other_reason'];
    }
    if(!empty($ar_other_reasons_field['FIELD_NAME'])){
        $fields[$ar_other_reasons_field['FIELD_NAME']] = $other_reason;
    }
}

// поле бренд
$ar_brand_field = false;
if(!empty($_REQUEST['brand_field'])){
    $ar_brand_field = \CAllUserTypeEntity::GetList(
        ['ID' => 'ASC'],
        ['ENTITY_ID' => 'CRM_LEAD','ID' => (int)$_REQUEST['brand_field']]
    )->Fetch();

    if(!empty($ar_brand_field['FIELD_NAME'])){
        $fields[$ar_brand_field['FIELD_NAME']] = $name;
    }
}

$lead_title = '';
if($no_target){
    $lead_title = $_REQUEST['phone'];
    // формируем название лида из списка выбранных причин

    foreach ($_REQUEST['reasons_list'] as $id){

        if($id == $reason_other_val['ID']){
            $lead_title .= ', '.$ar_reason_call_list[$id]['VALUE'].': '.$other_reason;
        }else{
            $lead_title .= ', '.$ar_reason_call_list[$id]['VALUE'];
        }
    }

}elseif(!empty($comment)){
    if(!empty($name)){
        $lead_title = $name.', ';
    }
    $lead_title .= $comment;
}elseif (!empty($other_reason)){
    $lead_title = $other_reason;
}else{
    $lead_title = $_REQUEST['phone'];
}

$fields['TITLE'] = $lead_title;
//$fields['NAME'] = $_REQUEST['name'];
$fields['COMMENTS'] = $comment;
$fields['ASSIGNED_BY_ID'] = $USER->GetID();
$fields['FM'] = ['PHONE' => ['n0' => ['VALUE' => $_REQUEST['phone'], 'VALUE_TYPE' => 'WORK'] ] ] ;

if($no_target){
    $fields['STATUS_ID'] = 'JUNK';
}

if(empty($_REQUEST['phone'])){
    $result['errors']['phone'] = true;
}

// если лид целевой, комментарий обязательное поле
if(empty($comment) && !$no_target){
    $result['errors']['comment'] = true;
}

if($_REQUEST['action'] == 'create' && !isset($_REQUEST['lead_id'])){

    if(empty($result['errors'])){

        $ar_multy = false;
        if(!empty($phone)){
            $ar_multy = \CCrmFieldMulti::GetList(['ID'=>'DESC'],['ENTITY_ID'=>'LEAD','TYPE_ID'=>'PHONE','=VALUE'=>$phone])->Fetch();
        }

        $ar_lead = false;
        if(!empty($ar_multy['ELEMENT_ID'])){
            $filter_lead = [
                'ID' => $ar_multy['ELEMENT_ID'],
                'CHECK_PERMISSIONS' => 'N',
                '!STATUS_ID' => ['CONVERTED','JUNK'] // CONVERTED - закончена работа, JUNK - отменен
            ];
            $ar_lead = \CCrmLead::GetList(['ID'=>'DESC'], $filter_lead)->Fetch();
        }

        // если лид по номеру найден, не позволяем создать новый, возвращаем найденный
        if(!empty($ar_lead) && !$no_target){
            $id = $ar_lead['ID'];
            $result['mode'] = 'fined';
        }else{
            $id = $crm_lead->Add($fields, true);
            $result['mode'] = 'create';
        }
    }
    $result['action'] = 'create';

}elseif ($_REQUEST['action'] == 'create' && !empty($_REQUEST['lead_id'])){
    if(empty($result['errors'])){
        $id = (int)$_REQUEST['lead_id'];
        if(\CCrmLead::GetList(['ID'=>'DESC'],['ID'=>$id, 'CHECK_PERMISSIONS' => 'N'])->Fetch()){
            $ar_multy = \CCrmFieldMulti::GetList(['ID'=>'DESC'],['ENTITY_ID'=>'LEAD','TYPE_ID'=>'PHONE','=VALUE'=>$phone,'ELEMENT_ID'=>$id])->Fetch();
            if(!empty($ar_multy)){
                // такой телефон уже существует в этом лиде
                unset($fields['FM']);
            }
            $crm_lead->Update($id, $fields);
        }
    }
    $result['action'] = 'update';
}elseif ($_REQUEST['action'] == 'get' && !empty($phone)){
    $ar_multy = \CCrmFieldMulti::GetList(['ID'=>'DESC'],['ENTITY_ID'=>'LEAD','TYPE_ID'=>'PHONE','=VALUE'=>$phone])->Fetch();
    $ar_lead = false;
    if(!empty($ar_multy['ELEMENT_ID'])){
        $ar_lead = \CCrmLead::GetList(['ID'=>'DESC'],['ID' => $ar_multy['ELEMENT_ID'], 'CHECK_PERMISSIONS' => 'N'])->Fetch();
        $ar_lead['reasons_list'] = [];
        if(!empty($ar_reason_call_field) && !empty($ar_lead[$ar_reason_call_field['FIELD_NAME']])){
            $db_reason_list = \CUserFieldEnum::GetList(['SORT' => 'ASC'],['ID' =>  $ar_lead[$ar_reason_call_field['FIELD_NAME']]]);
            while ($ar_val = $db_reason_list->Fetch()){
                $ar_lead['reasons_list'][$ar_val['ID']] = $ar_val['VALUE'];
            }
        }

        $ar_lead['target'] = '';
        if(!empty($ar_target_field) && !empty($ar_lead[$ar_target_field['FIELD_NAME']])){
            $ar_lead['target'] = $ar_lead[$ar_target_field['FIELD_NAME']];
        }

        $ar_lead['other_reason'] = '';
        if(!empty($ar_other_reasons_field) && !empty($ar_lead[$ar_other_reasons_field['FIELD_NAME']])){
            $ar_lead['other_reason'] = $ar_lead[$ar_other_reasons_field['FIELD_NAME']];
        }

        $ar_lead['brand'] = '';
        if(!empty($ar_brand_field) && !empty($ar_lead[$ar_brand_field['FIELD_NAME']])){
            $ar_lead['brand'] = $ar_lead[$ar_brand_field['FIELD_NAME']];
        }
    }
    $result['lead'] = $ar_lead;
    $result['action'] = 'get';
    $result['errors'] = [];
}

if((int)$id > 0){
    $result['lead'] = CCrmLead::GetList(['ID'=>'DESC'],['ID'=>$id, 'CHECK_PERMISSIONS' => 'N'])->Fetch();
}

$result['fields'] = $fields;

echo json_encode($result);




