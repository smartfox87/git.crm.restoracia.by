/*
	События на document:
		zvonkoInitialized - Zvonko загружен и все обработчики инициализированы
		zvonkoOpened - виджет открыт
		zvonkoClosed - виджет закрыт
		zvonkoCountdownStart - начало отсчета ожидания звонка
		zvonkoCountdownTimeUp - время ожидания звонка вышло
		zvonkoCountdownReset - сброс формы звонка в начальное состояние
*/

var Zvonko = new (function () {
	var inst = this;
	var $;


	inst.callbackCountdownCount = 0;
	inst.callbackCountdownTimestamp = null;
	inst.callbackCountdownInterval = null;
	inst.callbackCountdownTimeout = null;
	inst.callbackCountdownTarget = null;

	inst.touchMode = (function () {
		var check = false;
		(function (a) {
			if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
		})(navigator.userAgent || navigator.vendor || window.opera);
		return check;
	})();


	// initialization start
	inst.init = function () {
		if (typeof jQuery === 'undefined') {
			console.warn('Zvonko: jQuery not found. Loading...');

			var script = document.createElement('script');
			script.src = 'https://code.jquery.com/jquery-3.2.1.min.js';
			script.integrity = 'sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=';
			script.crossorigin = 'anonymous';
			script.onload = inst.load;
			document.body.appendChild(script);
		} else {
			inst.load();
		}
	}


	// load HTML
	inst.load = function () {
		$ = jQuery;

		if ($('.js-zvonko-btn').length) {
			inst.initEvents();
		}
	}


	// event initialization
	inst.initEvents = function () {
		var win = $(window);
		var body = $(document.body);
		var doc = $(document);


		$('.js-zvonko').toggleClass('zvonko-touch', inst.touchMode);
		$('.js-zvonko').toggleClass('zvonko-no-touch', !inst.touchMode);


		// prevent buggy input handling (hello bitronic)
		$(document).ready(function () {
			setTimeout(function () {
				$('.js-zvonko input').off('focus');
			}, 2000);

			setTimeout(function () {
				$('.js-zvonko input').off('focus');
			}, 4000);
		});


		// zvonko button click
		body.off('click.zvonkoOpenByClick', '.js-zvonko-btn')
			.on('click.zvonkoOpenByClick', '.js-zvonko-btn', function () {
				inst.open();
			});


		// click out of zvonko
		body.off('click.zvonkoCloseByClickOut')
			.on('click.zvonkoCloseByClickOut', function (e) {
				var target = $(e.target);
				if (target.closest('.js-zvonko').length === 0 || target.closest('.js-zvonko-close').length > 0)
					inst.close();
			});


		// toggle accordions
		body.off('click.zvonkoToggleAccordion', '.js-zvonko-accordion__bar')
			.on('click.zvonkoToggleAccordion', '.js-zvonko-accordion__bar', function () {
				var accordionItem = $(this).closest('.js-zvonko-accordion__item');
				var accordion = accordionItem.closest('.js-zvonko-accordion');

				if (!accordionItem.hasClass('open')) {
					accordion.find('.js-zvonko-accordion__item.open').removeClass('open').find('.js-zvonko-accordion__body').css('display', 'block').slideUp(500);
					accordionItem.find('.js-zvonko-accordion__body').slideDown(500).end().addClass('open');
				}
			});

		body.off('click.zvonkoToggleAccordionByLabel', '.js-zvonko-label')
			.on('click.zvonkoToggleAccordionByLabel', '.js-zvonko-label', function () {
				var t = $(this);

				t.closest('.js-zvonko').find('.js-zvonko-accordion__item, .js-zvonko-label').removeClass('open').filter('[data-telephony-part="' + t.attr('data-telephony-part') + '"]').addClass('open');
			});


		$('.js-zvonko-format-tel').off('change.zvonkoFormatTel keyup.zvonkoFormatTel keydown.zvonkoFormatTel paste.zvonkoFormatTel focus.zvonkoFormatTel')
			.on('change.zvonkoFormatTel keyup.zvonkoFormatTel keydown.zvonkoFormatTel paste.zvonkoFormatTel focus.zvonkoFormatTel', function () {
				var hardcoded = this.dataset.hardTelCode;
				if (this.value.indexOf(hardcoded) !== 0) {
					this.value = hardcoded;
					var that = this;
					setTimeout(function () {
						that.selectionStart = that.selectionEnd = 10000;
					}, 0);
				}
			});


		$('.js-zvonko-format-tel').off('keydown.inputHelperNumber').on('keydown.inputHelperNumber', function (e) {
			var keyID = (window.event) ? event.keyCode : e.keyCode;

			if ((keyID < 48 || keyID > 57) && (keyID < 96 || keyID > 105) && keyID !== 8 && keyID !== 9 &&
				keyID !== 46 && keyID !== 116 && keyID !== 37 && keyID !== 39 && keyID !== 13 /* space && keyID !== 32*/) {
				return false;
			}
		});


		// rich text input
		body.off('change.zvonkoToggleRTI blur.zvonkoToggleRTI focus.zvonkoToggleRTI zvonkoRefreshRTI', '.js-zvonko-rti__input')
			.on('change.zvonkoToggleRTI blur.zvonkoToggleRTI focus.zvonkoToggleRTI zvonkoRefreshRTI', '.js-zvonko-rti__input', function (e) {
				var t = $(this);
				var hasValue = this.value.length > 0;

				if (e.type !== 'focusin' && t.hasClass('js-zvonko-format-tel') && this.value === this.dataset.hardTelCode)
					hasValue = false;

				t.closest('.js-zvonko-rti').toggleClass('zvonko-rti--active', e.type === 'focusin' || hasValue);
			});
		$('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');


		// open bitrix 24 chat
		body.off('click.zvonkoOpenB24', '.js-zvonko-open-b24')
			.on('click.zvonkoOpenB24', '.js-zvonko-open-b24', function () {
				try {
					inst.close();
					BX.LiveChat.openLiveChat();
				} catch (e) {
					console.log('Zvonko error: can not open bitrix 24 chat');
				}
			});


		// open zendesk chat
		body.off('click.zvonkoOpenZendesk', '.js-zvonko-open-zendesk')
			.on('click.zvonkoOpenZendesk', '.js-zvonko-open-zendesk', function () {
				try {
					inst.close();
					$zopim.livechat.window.show();
				} catch (e) {
					console.log('Zvonko error: can not open Zendesk chat');
				}
			});


		// open zendesk chat
		body.off('click.zvonkoOpenJivosite', '.js-zvonko-open-jivosite')
			.on('click.zvonkoOpenJivosite', '.js-zvonko-open-jivosite', function () {
				try {
					inst.close();
					jivo_api.open()
				} catch (e) {
					console.log('Zvonko error: can not open Zendesk chat');
				}
			});


		// submit form
		body.off('submit.zvonkoSubmitMsgForm', '.js-zvonko-form')
			.on('submit.zvonkoSubmitMsgForm', '.js-zvonko-form', function (e) {
				var form = $(this);
				var data = {};


				// grab data
				if (form.hasClass('js-zvonko-form--call')) {
					data = {
						type: 'call',
						telephone: form.find('[name="telephone"]').val(),
						timeout: parseInt($('.js-zvonko-call-countdown-target').attr('data-zvonko-call-countdown'))
					}
				} else if (form.hasClass('js-zvonko-form--msg')) {
					data = {
						type: 'message',
						name: form.find('[name="name"]').val(),
						telephone: form.find('[name="telephone"]').val(),
						email: form.find('[name="email"]').val(),
						message: form.find('[name="message"]').val()
					}
				}

				e.preventDefault();
			});


		// submit form
		body.off('reset.zvonkoResetMsgForm', '.js-zvonko-form')
			.on('reset.zvonkoResetMsgForm', '.js-zvonko-form', function (e) {
				e.preventDefault();
				var form = $(this);
				form.add(form.find('.js-zvonko-form-row')).removeClass('zvonko-state-sending zvonko-state-success zvonko-state-error');
				form.find('.js-zvonko-clear-on-reset').val('').trigger('zvonkoRefreshRTI');

				if (!inst.touchMode)
					form.find('.js-zvonko-focus-on-reset').focus();
			});

		body.off('mouseenter.button_clear', '.js-zvonko-rti')
			.on('mouseenter.button_clear', '.js-zvonko-rti', function () {

				if ($(this).find('.clear-button-input').length) {
					$(this).find('.clear-button-input').css({'position': 'absolute'});
				}
			})

		body.off('mouseleave.button_clear', '.js-zvonko-rti')
			.on('mouseleave.button_clear', '.js-zvonko-rti', function () {
				if ($(this).find('.clear-button-input').length) {
					$(this).find('.clear-button-input').css({'position': ''});
				}
			})

		body.off('click.button_clear', '.clear-button-input')
			.on('click.button_clear', '.clear-button-input', function () {
				$('#telephony_title').empty();
				$(this).closest('form').find('input').each(function () {
					$(this).val('');
				})
				$(this).closest('form').find('.user-name-container').hide();
				$(this).closest('form').find('input[name=bonus_cart]').siblings('.js-zvonko-paste-error-here').html('бонусная карта');
			})


		setTimeout(function () {
			$('.js-zvonko').css('display', '');
		}, 0);


		doc.trigger('zvonkoInitialized');
	}


	inst.open = function () {
		$(document.body).addClass('zvonko-widget-open');
		$(document).trigger('zvonkoOpened');
	}


	inst.close = function () {
		$(document.body).removeClass('zvonko-widget-open');
		$(document).trigger('zvonkoClosed');
	}


	inst.startCallbackCountdown = function () {
		inst.callbackCountdownTarget = $('.js-zvonko-call-countdown-target');
		inst.callbackCountdownCount = parseInt(inst.callbackCountdownTarget.attr('data-zvonko-call-countdown'));
		inst.callbackCountdownResetCount = parseInt(inst.callbackCountdownTarget.attr('data-zvonko-call-reset'));
		inst.callbackCountdownTimestamp = +new Date();
		clearInterval(inst.callbackCountdownInterval);
		inst.callbackCountdownInterval = setInterval(inst.callbackCountdownHandler, 250);
		clearTimeout(inst.callbackCountdownTimeout);

		$(document).trigger('zvonkoCountdownStart');
	}


	inst.endCallbackCountdown = function () {
		clearInterval(inst.callbackCountdownInterval);
		$('.js-zvonko-form').addClass('zvonko-state-time-up');

		$(document).trigger('zvonkoCountdownTimeUp');
	}


	inst.resetCallbackCountdown = function () {
		$('.js-zvonko-form--call, .js-zvonko-form--call .js-zvonko-form-row').removeClass('zvonko-state-time-up zvonko-state-sending zvonko-state-success zvonko-state-error');

		$(document).trigger('zvonkoCountdownReset');
	}


	inst.callbackCountdownHandler = function () {
		var delta = inst.callbackCountdownCount - Math.round((new Date() - inst.callbackCountdownTimestamp) / 1000);
		if (delta <= 0) {
			inst.endCallbackCountdown();
			clearTimeout(inst.callbackCountdownTimeout);
			inst.callbackCountdownTimeout = setTimeout(inst.resetCallbackCountdown, inst.callbackCountdownResetCount * 1000);
		} else {
			inst.callbackCountdownTarget.html(delta);
		}
	}
});


if (document.readyState != 'loading') {
	Zvonko.init();
} else {
	document.addEventListener('DOMContentLoaded', Zvonko.init);
}