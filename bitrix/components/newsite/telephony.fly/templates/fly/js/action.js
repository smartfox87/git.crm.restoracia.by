document.addEventListener('DOMContentLoaded', telephony_run);

var UF_DEBT = 'UF_CRM_1550236593';
var UF_ARREARS = 'UF_CRM_1550236636';
var UF_SHIPMENT_STATUS = 'UF_CRM_1550229507';
var UF_UNP = 'UF_CRM_1550136152571';

function telephony_run() {

	BX.addCustomEvent('onPullEvent', function (module_id, command, params) {
		//console.log(params.CALL.CALLER_ID);
		var phone = '';

		if (module_id == 'telephony' && command == 'message') {

			if (typeof Zvonko == 'object') {
				Zvonko.open();

				if (typeof params.fields != 'undefined') {
					var title_container = $('#telephony_title');
					title_container.html('');
					var title = '';
					var params_obj = {'entity_id': params.fields.CRM_ENTITY_ID};


					if (typeof params.ENTITY_DATA.PHONE != 'undefined' && params.ENTITY_DATA.PHONE != '') {
						phone = params.ENTITY_DATA.PHONE;
					}

					if (params.fields.CRM_ENTITY_TYPE == 'CONTACT' && params.fields.CRM_ENTITY_ID > 0) {
						params_obj.entity = 'contact';
						get_contact_data(params_obj);
						title = get_detail_link(params.fields.CRM_ENTITY_ID, 'contact', params.ENTITY_DATA.CONTACT.FULL_NAME);
						title += '<br>(контакт ' + params.fields.CRM_ENTITY_ID + ')';
						get_contact_deal_statistic(params.fields.CRM_ENTITY_ID);

						var form_auth = $('#telephony_auth_form');

						if (params.ENTITY_DATA.BUS_USER_ID && form_auth.length) {
							get_user_data_bus(params.ENTITY_DATA.BUS_USER_ID, form_auth);
						}

					} else if (params.fields.CRM_ENTITY_TYPE == 'LEAD' && params.fields.CRM_ENTITY_ID > 0) {
						params_obj.entity = 'lead';
						get_lead_data(params_obj);
						title = get_detail_link(params.fields.CRM_ENTITY_ID, 'lead', params.ENTITY_DATA.LEAD.TITLE);
						title += '<br>(лид ' + params.fields.CRM_ENTITY_ID + ')';
					} else if (params.fields.CRM_ENTITY_TYPE == 'COMPANY' && params.fields.CRM_ENTITY_ID > 0) {
						params_obj.entity = 'company';
						get_company_data(params_obj);
						//title = params.ENTITY_DATA.COMPANY.TITLE;
						title = get_detail_link(params.fields.CRM_ENTITY_ID, 'company', params.ENTITY_DATA.COMPANY.TITLE);
						//title += '<br>(компания ' + params.fields.CRM_ENTITY_ID + ')';
						if (params.ENTITY_DATA.COMPANY[UF_DEBT]) {
							title += '<br>Общая задолженность: ' + params.ENTITY_DATA.COMPANY[UF_DEBT];
						}
						if (params.ENTITY_DATA.COMPANY[UF_ARREARS]) {
							title += '<br>Просроченная: ' + params.ENTITY_DATA.COMPANY[UF_ARREARS];
						}
						if (params.ENTITY_DATA.COMPANY[UF_ARREARS]) {
							var shipment_status = 'Не выбран';
							if (params.ENTITY_DATA.COMPANY[UF_SHIPMENT_STATUS] === '58') {
								shipment_status = 'Разрешена';
							} else if (params.ENTITY_DATA.COMPANY[UF_SHIPMENT_STATUS] === '59') {
								shipment_status = 'Запрещена';
							}
							title += '<br>Статус отгрузки: ' + shipment_status;
						}
					} else {
						$(document).find('div[data-telephony-part=statistic]').hide();
						$(document).find('div[data-telephony-part=interests]').hide();
						$(document).find('div[data-telephony-part=notes]').hide();
					}

					// заполнение формы авторизации
					if (params.fields.CALLER_ID != '') {
						init_auth_register_form(params);
					}

					if (title != '') {
						title_container.html(title);
					}

					// инициализация формы создания лида

					if (phone != '') {
						init_form_lead_create(phone);
					}
				}

				init_events();
			}
		}
	});

	init_events();
}

function close_tab() {
	$('.js-zvonko-close').trigger('click');
}

function dopInfo(param) {

	var title_container = $('#telephony_title');
	$.ajax({
		url: '/bitrix/components/newsite/telephony.fly/templates/fly/ajax/get_dop_info_crm.php',
		data: ({'INN': param}),
		dataType: 'json',
		success: function (response) {
			console.log(response);
			title = response.NAME;
			//title += response.ID;
			title += response.UF_DEBT;
			title += response.UF_ARREARS;
			title += response.STATUS;
			title_container.html(title);
		}
	});

}

function RestInfoInsert() {

	var n = document.getElementById('user_variant').options.selectedIndex;
//console.log(document.getElementById("user_variant").options[n].getAttribute('data-id'));
	var form = $('#telephony_auth_form');
	form.find('input[name=NAME]').val(document.getElementById('user_variant').options[n].getAttribute('data-name'));


	if (document.getElementById('user_variant').options[n].getAttribute('data-inn') != 'undefined') {
		form.find('input[name=INN]').val(document.getElementById('user_variant').options[n].getAttribute('data-inn'));
		dopInfo(document.getElementById('user_variant').options[n].getAttribute('data-inn'));
	} else {
		form.find('input[name=INN]').val('');
		$('#telephony_title').html('');
	}
	form.find('input[name=user_id]').val(document.getElementById('user_variant').options[n].getAttribute('data-id'));

	form.find('input[name=phone]').val(document.getElementById('user_variant').options[n].getAttribute('data-phone'));
	form.find('input[name=user_name]').val(document.getElementById('user_variant').options[n].getAttribute('data-username'));
	form.find('.user-name-container').show();

	form.find('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');
}

function init_events() {

	$('.number-only').unbind('change keyup input click').bind('change keyup input click', function () {
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
	});

	$(document).off('click.telephony').on('click.telephony', 'button.zvonko-btn.zvonko-btn--primary', function (e) {
		e.preventDefault();

		var form = $(this).closest('form');
		var data = form.serializeArray();

		if (form.attr('id') == 'telephony_auth_form') { // обработка отправки авторизации

			var send = false;
			var button_name = $(this).attr('name');
			var mode = $(this).attr('value');

			if (!$.isEmptyObject(data) && button_name == 'mode') {

				if (mode == 'auth') {
					var link_auth = '';

					link_auth += 'https://';
					link_auth += form.find('input[name=auth_link]').val() != '' ? form.find('input[name=auth_link]').val() : 're-store.by';
					link_auth += '/b24_exchange/?action=login_crm&remote_ip=' + remote_ip;

					if (manager_id != undefined) {
						link_auth += '&manager_id=' + manager_id;
					}

					var user_id = form.find('input[name=user_id]').val() ? form.find('input[name=user_id]').val() : false;
					if (user_id) {
						link_auth += '&user_id=' + user_id;
						send = true;
					}

					var bonus_cart = form.find('input[name=bonus_cart]').val() ? form.find('input[name=bonus_cart]').val() : false;
					if (bonus_cart) {
						link_auth += '&bonus_cart=' + bonus_cart;
						send = true;
					}

					var phone = form.find('input[name=phone]').val() ? form.find('input[name=phone]').val() : false;
					if (phone) {
						link_auth += '&phone=' + phone;
						send = true;
					}

					if (send) {
						window.open(link_auth);
					}
				} else if (mode == 'get') {
					data.push({name: 'mode', value: mode});

					$.ajax({
						url: url_get_user_data,
						data: data,
						dataType: 'json',
						success: function (response) {

							var json = JSON.parse(response);
							console.log('jjjjjjjjjjjjjjjjjjjjjjj', json)


							var title_container = $('#telephony_title');
							if (json.error != undefined && json.error != '') {
								// ошибка доступа
								var title = json.error + '<br>' + json.remote_ip;
								title_container.html(title);
								//console.log(json);
								return false;
							} else {
								title_container.html('');
							}

							var isset_user = false;
							if (Object.keys(json).length == 1) {
								for (var key in json) {
									var json = json[key];
									dopInfo(json.INN);

									$('#select_input, #seweral_comp').empty();
								}

							} else if (Object.keys(json).length > 1) {
								$('#seweral_comp').html('Найдено ' + Object.keys(json).length + ' компаний');

								var html = '<select id="user_variant" onchange="RestInfoInsert()">';
								var insert = $('#select_input');
								var key_el = 0;
								for (var key in json) {
									if (key_el == 0) {
										key_el = key;
									}
									var res = json[key];
									var phone = res.PERSONAL_MOBILE ? res.PERSONAL_MOBILE : (res.WORK_PHONE ? res.WORK_PHONE : res.phone);
									var name = res.NAME;
									if (res.LAST_NAME != undefined) {
										name += ' ' + res.LAST_NAME;
									}

									html += '<option data-username="' + name + '" data-name="' + res.NAME + '" data-inn="' + res.INN + '" data-id="' + res.ID + '" data-phone="' + phone + '">' + res.NAME + '</option>';


								}
								html += '</select>';
								insert.html(html);
								isset_user = true;
								var json = json[key_el];
							}


							var phone = json.PERSONAL_MOBILE ? json.PERSONAL_MOBILE : (json.WORK_PHONE ? json.WORK_PHONE : json.phone);
							var show_register_form = false;
							if (json.NAME) {
								form.find('input[name=NAME]').val(json.NAME);
							}
							if (json.INN) {

								form.find('input[name=INN]').val(json.INN);
							}
							if (json.NAME) {
								var name = json.NAME;
								if (json.LAST_NAME != undefined) {
									name += ' ' + json.LAST_NAME;
								}
								form.find('input[name=user_name]').val(name);
								form.find('.user-name-container').show();
							} else {
								form.find('input[name=user_name]').val('');
								form.find('.user-name-container').hide();
							}

							if (json.ID) {
								form.find('input[name=user_id]').val(json.ID);
							} else {
								show_register_form = true;
								form.find('input[name=user_id]').val('');
							}

							if (json.UF_BONUS_CARD_NUM) {
								form.find('input[name=bonus_cart]').val(json.UF_BONUS_CARD_NUM);
							} else {
								form.find('input[name=bonus_cart]').val('');
							}

							if (json.UF_LOGICTIM_BONUS) {
								form.find('input[name=bonus_cart]').siblings('.js-zvonko-paste-error-here').html('бонусная карта (баллов ' + json.UF_LOGICTIM_BONUS + ')');
							} else {
								form.find('input[name=bonus_cart]').siblings('.js-zvonko-paste-error-here').html('бонусная карта');
							}

							if (phone) {
								form.find('input[name=phone]').val(phone);
							} else {
								form.find('input[name=phone]').val('');
							}

							if (show_register_form && !isset_user) {
								// такого пользователя нет, подымаем форму регистрации
								$('div.js-zvonko-accordion__item[data-telephony-part=register_form]').find('.js-zvonko-accordion__bar').click();
								// прячем кнопку авторизации
								form.find('button[value=auth]').hide()
							} else {
								// открываем форму с авторизацией
								$('div.js-zvonko-accordion__item[data-telephony-part=auth_form]').find('.js-zvonko-accordion__bar').click();
								if (form.find('.js-zvonko-rti__input').length) { // инициальзация заполненых инпутов формы
									form.find('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');
									dopInfo(json.INN);
								}
								// показываем кнопку авторизации
								form.find('button[value=auth]').show();
							}
							$('div.js-zvonko-accordion__item[data-telephony-part=register_form]').find('.form-error-response').html('');
						}
					})
				}
			}
		} else if (form.attr('id') == 'telephony_register_form') { // обработка формы регистрации нового пользователя БУС

			if (!$.isEmptyObject(data)) {

				for (var i = 0; i < data.length; i++) {
					var block_error = form.find('input[name=' + data[i].name + ']').parents('.zvonko-form-row').find('.zvonko-form-error-msg');
					if (data[i].value == '') {
						block_error.show();
						return;
					} else {
						block_error.hide();
					}
				}
				var url = $(form).attr('action');
				//console.log(data)
				$.ajax({
					url: url,
					data: data,
					//dataType : 'json',
					success: function (response) {

						var json = JSON.parse(response);
						//console.log(json)
						var form_auth = $('div.js-zvonko-accordion__item[data-telephony-part=auth_form]');
						var form_register = $('div.js-zvonko-accordion__item[data-telephony-part=register_form]');
						if (json.error != '') {
							form_register.find('.form-error-response').html(json.error);
						} else {
							form_register.find('.form-error-response').html('');
							form_register.find('.contact-conversion-message').hide();
							// добавляем в форму авторизации id пользователя
							form_auth.find('input[name=user_id]').val(json.user_id);
							form_auth.find('input[name=phone]').val(json.user_data.PERSONAL_MOBILE);
							form_auth.find('input[name=user_name]').val(json.user_data.NAME);
							form_auth.find('.user-name-container').show();

							if (json.contact_id > 0) {
								$('.contact-conversion-message').show();
								$('.contact-conversion-message strong').html(json.contact_id);
							}

							// устанавливаем заголовок формы из нового контакта
							var title_container = $('#telephony_title');
							title_container.html('');
							//var title = json.user_data.NAME;
							var title = get_detail_link(json.contact_id, 'contact', json.user_data.NAME);
							title += '<br>(контакт ' + json.contact_id + ')';
							title_container.html(title);

							setTimeout(function () {
								// открываем форму с авторизацией
								if (form_auth.find('.js-zvonko-rti__input').length) { // инициальзация заполненых инпутов формы
									form_auth.find('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');
								}

								form_auth.find('.js-zvonko-accordion__bar').click();
								//$('.contact-conversion-message').hide();
							}, 3000)
						}

					}
				})
					.fail(function (error) {
						console.log('error', error);
					})

			}

		} else if (form.attr('id') == 'telephony_create_lead_form') { // обработка форма создания лида

			var url = $(form).attr('action');
			data.push({name: 'action', value: 'create'});

			$.post(url, data, function (json) {
				var response = JSON.parse(json);
				remove_notifications(form);
				clear_errors(form);
				//console.log(response);
				var has_error = false;
				// подсветка ошибок
				if (!$.isEmptyObject(response.errors)) {
					for (key in response.errors) {
						$(form).find('[name=' + key + ']').closest('.js-zvonko-form-row').addClass('zvonko-state-error')
					}
					form.append(write_error_block('Заполните обязательные поля'));
					show_message('error', form, 1000);
					has_error = true;
				}

				if (typeof response.lead.ID != 'undefined' && parseInt(response.lead.ID) > 0 && !has_error) {
					if (response.action == 'create') {

						form.find('button[name=action]').html('Обновить лид #' + response.lead.ID);

						if (response.mode == 'create') {
							form.append(write_success_block('Лид #' + response.lead.ID + ' успешно добалвен.'));
							show_message('success', form, 3000);
						} else if (response.mode == 'fined') {
							form.append(write_error_block('Найден Лид #' + response.lead.ID + ' с данным номером телефона!'));
							form.find('input[name=name]').val(response.lead.NAME);
							form.find('textarea[name=comment]').val(response.lead.COMMENTS);
							show_message('error', form, 3000);
							//form.find('button[name=action]').remove();
						}

						if (form.find('input[name=lead_id]').length) {
							form.find('input[name=lead_id]').val(response.lead.ID);
						} else {
							form.append('<input type="hidden" name="lead_id" value="' + response.lead.ID + '">');
						}

						var detail_link = get_detail_link(response.lead.ID, 'lead', 'Лид #' + response.lead.ID);
						if ($('#detail_link_lead').length) {
							$('#detail_link_lead').html(detail_link);
						}
					} else if (response.action == 'update') {
						form.append(write_success_block('Лид #ID ' + response.lead.ID + ' успешно обновлен.'));
						show_message('success', form, 3000);
					}
				}

				form.find('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');
			})

		} else { // обработка отправки заметки и списка интересов

			var url = $(form).attr('action');
			var entity = $(form).find('input[name=entity]').val();

			if (!$.isEmptyObject(data)) {
				data.push({'name': 'action', 'value': 'update'});
				$.post(url, data, function (json) {

					var response = JSON.parse(json);
					//console.log(response);
					if (typeof response.success[entity]['list'] != 'undefined') {
						if (response.success[entity]['list'] === true) {
							show_message('success', form);
						} else if (response.success[entity]['list'] === false) {
							show_message('error', form);
						}
					}


					if (typeof response.success[entity]['notes'] != 'undefined') {
						if (response.success[entity]['notes'] === true) {
							show_message('success', form);
							$('textarea[name="notes"]').val('');
						} else if (response.success[entity]['notes'] === false) {
							show_message('error', form);
							$('textarea[name="notes"]').val('');
						}
					}

					if (response.reload == 1) {
						// setTimeout(function () {
						//     window.location.reload();
						// },3000);
					}
				})
			}
		}

	})

	init_lead_select();
}

function get_user_data_bus(user_id, form) {
	var data = {'user_id': user_id, 'mode': 'get'};
	console.log(url_get_user_data);
	$.ajax({
		url: url_get_user_data,
		data: data,
		dataType: 'json',
		success: function (response) {
			var json = JSON.parse(response);
			//console.log(json)

			if (json.NAME) {
				var name = json.NAME;
				if (json.LAST_NAME != undefined) {
					name += ' ' + json.LAST_NAME;
				}
				form.find('input[name=user_name]').val(name);
				form.find('.user-name-container').show();
			} else {
				form.find('input[name=user_name]').val('');
				form.find('.user-name-container').hide();
			}

			// открываем форму с авторизацией
			$('div.js-zvonko-accordion__item[data-telephony-part=auth_form]').find('.js-zvonko-accordion__bar').click();

			if (form.find('.js-zvonko-rti__input').length) { // инициальзация заполненых инпутов формы
				form.find('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');
			}
		}
	})
}

function get_detail_link(item_id, entity, name) {
	var link = '<a target="_blank" href="/crm/' + entity + '/show/' + item_id + '/">' + name + '</a>';
	return link;
}

// нужна, работает
function write_checkboxes(entity, list) {
	var result = '<div class="zvonko-form-row js-zvonko-form-row">' +
		'<ul data-entity="' + entity + '" class="ul-card-show">';
	var counter = 0;
	for (id in list) {
		if (counter == 0) {
			result += '<input name="field_name" value="' + list[id]['field_name'] + '" type="hidden">';
		}
		var selected = list[id]['selected'] ? 'checked' : '';
		result += '<li data-id="' + list[id]['id'] + '" ><label><input type="checkbox" ' + selected + '  name="' + list[id]['field_name'] + '[]" value="' + list[id]['id'] + '">' + list[id]['name'] + '</label></li>';
		counter++;
	}
	result += '</ul></div>';
	return result;
}

// нужна, работает
function write_button(button_title) {
	var button = '<div class="zvonko-form-row zvonko-form-row--offset"><button type="submit" class="zvonko-btn zvonko-btn--primary">' + button_title + '</button></div>';
	return button;
}

// нужна, работает
function write_textarea(placeholder) {
	placeholder = placeholder ? placeholder : 'Сообщение';
	var str = '<div class="zvonko-form-row js-zvonko-form-row"><label class="zvonko-rti js-zvonko-rti">' +
		'<textarea class="zvonko-rti__input zvonko-input js-zvonko-rti__input js-zvonko-clear-on-reset js-zvonko-focus-on-reset" name="notes"></textarea>' +
		'<span class="zvonko-rti__label js-zvonko-paste-error-here">' + placeholder + '</span>' +
		'</label></div>';
	return str;
}

// нужна, работает
function write_success_block(message) {
	return '<div class="zvonko-form-msg zvonko-form-msg--success">' + message + '</div>'
}

// очистка уведомлений в форме
function remove_notifications(scope) {
	if ($(scope).find('.zvonko-form-msg.zvonko-form-msg--success').length) {
		$(scope).find('.zvonko-form-msg.zvonko-form-msg--success').remove();
	}
	if ($(scope).find('.zvonko-form-msg.zvonko-form-msg--error').length) {
		$(scope).find('.zvonko-form-msg.zvonko-form-msg--error').remove();
	}
}

// очистка от ошибок
function clear_errors(form) {
	// очистка формы от ошибок
	for (var i = 0; i < $(form).find('.zvonko-form-row.js-zvonko-form-row').length; i++) {
		if ($($(form).find('.zvonko-form-row.js-zvonko-form-row')[i]).hasClass('zvonko-state-error')) {
			$($(form).find('.zvonko-form-row.js-zvonko-form-row')[i]).removeClass('zvonko-state-error')
		}
	}
}

// нужна, работает
function write_error_block(message) {
	return '<div class="zvonko-form-msg zvonko-form-msg--error">' + message + '</div>'
}

// нужна, работает
function show_message(type, scope, timer) {

	var add_class = '';

	switch (type) {
		case 'success':
			add_class = 'zvonko-state-success';
			break;
		case 'error':
			add_class = 'zvonko-state-error';
			break;
	}

	if (!$(scope).hasClass(add_class)) {
		//$(scope).addClass(add_class);
		$(scope).toggleClass(add_class);
	}

	if (typeof timer == 'undefined') {
		timer = 3000;
	}
	setTimeout(function () {
		//$(scope).removeClass(add_class);
		$(scope).toggleClass(add_class);
	}, timer);

}

function get_contact_deal_statistic(contact_id) {
	$.ajax({
		url: url_get_statistic,
		data: {'contact_id': contact_id},
		success: function (json) {
			var list = '';

			if (json != '') {
				var obj = JSON.parse(json)
				if (!$.isEmptyObject(obj)) {
					for (key in obj) {
						if (obj[key].DEAL_LIST != '') {
							list += '<li><b>' + obj[key].NAME + '</b> - ' + obj[key].PRICE + ' руб. (' + Object.size(obj[key].DEAL_LIST) + ')</li>';
						}
					}
				}
			}

			$(document).find('div[data-telephony-part=statistic]').find('ul.statistic-list').html(list);
			if (list != '') {
				$(document).find('div[data-telephony-part=statistic]').show();
			} else {
				$(document).find('div[data-telephony-part=statistic]').hide();
			}
		}
	})
}

function get_data(entity, entity_id) {
	var request_obj = {};
	switch (entity) {
		case 'contact':
			request_obj = {'contact_id': entity_id, 'action': 'get'};
			break;
		case 'lead':
			request_obj = {'lead_id': entity_id, 'action': 'get'};
			break;
		case 'company':
			request_obj = {'company_id': entity_id, 'action': 'get'};
			break;
	}

	if ($.isEmptyObject(request_obj)) {
		return;
	}

	$.post(url_card_show, request_obj, function (json) {

		var data = JSON.parse(json);
		var form_interest = $('form#telephony_interests_form');
		var form_notes = $('form#telephony_notes_form');


		if (data[entity]['PRODUCT_INTEREST'] != undefined && !$.isEmptyObject(data[entity]['PRODUCT_INTEREST'])) {
			var input_entity = '<input name="entity" value="' + entity + '" type="hidden">';
			var input_entity_id = '<input name="entity_id" value="' + entity_id + '" type="hidden">';
			form_interest.html('');
			form_interest.append(input_entity);
			form_interest.append(input_entity_id);
			form_interest.append(write_success_block('Список интересов успешно обновлен'));
			form_interest.append(write_error_block('Произошла ошибка обновления списка'));
			form_interest.append(write_checkboxes(entity, data[entity]['PRODUCT_INTEREST']));
			form_interest.append(write_button('Сохранить'));
			$(document).find('div[data-telephony-part=interests]').show();

			form_notes.html('');
			form_notes.append(input_entity);
			form_notes.append(input_entity_id);
			form_notes.append(write_success_block('Заметка успешно сохранена'));
			form_notes.append(write_error_block('Произошла ошибка сохранения заметки'));
			form_notes.append(write_textarea());
			form_notes.append(write_button('Сохранить'));
			$(document).find('div[data-telephony-part=notes]').show();
		}
	})
}

function get_contact_data(params) {
	get_data(params.entity, params.entity_id);
}

function get_lead_data(params) {
	get_data(params.entity, params.entity_id);
}

function get_company_data(params) {
	get_data(params.entity, params.entity_id);
}


function init_auth_register_form(params) {

	var auth_form = $('form#telephony_auth_form');
	auth_form.find('input').val('');

	var register_form = $('form#telephony_register_form');
	register_form.find('input').val('');

	var phone = '';

	if (typeof params.fields.CALLER_ID != 'undefined' && params.fields.CALLER_ID > 0) {
		phone = params.fields.CALLER_ID;
	} else if (typeof params.ENTITY_DATA.PHONE != 'undefined' && params.ENTITY_DATA.PHONE != '') {
		phone = params.ENTITY_DATA.PHONE;
	} else if (typeof params.CALL.CALLER_ID != 'undefined' && params.CALL.CALLER_ID != '') {
		phone = params.CALL.CALLER_ID;
		init_form_lead_create(phone);
	}

	if (typeof params.ENTITY_DATA.BUS_USER_ID != 'undefined') {
		auth_form.find('input[name=user_id]').val(params.ENTITY_DATA.BUS_USER_ID);
	}

	var company_inn = '';
	if ((typeof params.ENTITY_DATA.COMPANY.REQUISITE != 'undefined') && params.ENTITY_DATA.COMPANY.REQUISITE[0].RQ_INN) {
		company_inn = params.ENTITY_DATA.COMPANY.REQUISITE[0].RQ_INN;
	} else if (params.ENTITY_DATA.COMPANY[UF_UNP]) {
		company_inn = params.ENTITY_DATA.COMPANY[UF_UNP];
	}

	if (company_inn !== '') {
		auth_form.find('input[name=INN]').val(company_inn);
		register_form.find('input[name=INN]').val(company_inn);
	} else {
		auth_form.find('input[name=phone]').val(phone);
		register_form.find('input[name=phone]').val(phone);
	}


	if (typeof params.ENTITY_DATA.BONUS_CART != 'undefined' && params.ENTITY_DATA.BONUS_CART != false) {
		auth_form.find('input[name=bonus_cart]').val(params.ENTITY_DATA.BONUS_CART);
		if (typeof params.ENTITY_DATA.BONUS_POINTS != 'undefined' && params.ENTITY_DATA.BONUS_POINTS != false) {
			auth_form.find('input[name=bonus_cart]').siblings('.js-zvonko-paste-error-here').html('бонусная карта (баллов ' + params.ENTITY_DATA.BONUS_POINTS + ')');
		}
	} else {
		auth_form.find('input[name=bonus_cart]').siblings('.js-zvonko-paste-error-here').html('бонусная карта');
	}

	if (auth_form.find('.js-zvonko-rti__input').length) { // инициальзация заполненых инпутов формы
		auth_form.find('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');
	}

	// открываем форму с авторизацией
	$('div.js-zvonko-accordion__item[data-telephony-part=auth_form]').find('.js-zvonko-accordion__bar').click();

	if (register_form.find('.js-zvonko-rti__input').length) { // инициальзация заполненых инпутов формы
		register_form.find('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');
	}
	// очищаем ошибку формы регистрации
	$('div.js-zvonko-accordion__item[data-telephony-part=register_form]').find('.form-error-response').html('');
}

function init_form_lead_create(phone) {

	init_lead_select();

	var form = $('#telephony_create_lead_form');
	form.find('input[name=phone]').val(phone);

	var data = form.serializeArray();

	data.push({name: 'action', value: 'get'});
	data.push({name: 'phone', value: phone});


	$.post(url_action_lead, data, function (response) {
		var json = JSON.parse(response);
		//console.log(json);
		var title = 'Создать лид';

		if (typeof json.lead != 'undefined' && parseInt(json.lead.ID) > 0) {

			remove_notifications(form);
			clear_errors(form);

			if (typeof json.lead.brand != 'undefined') {
				form.find('input[name=name]').val(json.lead.brand);
			} else {
				form.find('input[name=name]').val(json.lead.NAME);
			}
			form.find('textarea[name=comment]').val(json.lead.COMMENTS);
			form.append(write_success_block('Лид #' + json.lead.ID + ' найден.'));
			show_message('success', form, 2000);

			if (form.find('input[name=lead_id]').length) {
				form.find('input[name=lead_id]').val(json.lead.ID);
			} else {
				form.append('<input type="hidden" name="lead_id" value="' + json.lead.ID + '">');
			}
			form.find('button[name=action]').html('Обновить лид #' + json.lead.ID)
			var detail_link = get_detail_link(json.lead.ID, 'lead', 'Лид #' + json.lead.ID);
			if ($('#detail_link_lead').length) {
				$('#detail_link_lead').html(detail_link);
			}

			if (typeof json.lead.other_reason != 'undefined') {
				form.find('textarea[name=other_reason]').val(json.lead.other_reason);
				if (json.lead.other_reason != '') {
					$('textarea[name=other_reason]').closest('label').show('fast');
				}
			}

			// установка целевого типа
			if (typeof json.lead.target != 'undefined') {
				if (form.find('input[name=target][value=' + json.lead.target + ']').length) {
					form.find('input[name=target][value=' + json.lead.target + ']').click();
				}
			}

			if (typeof json.lead.reasons_list != 'undefined') {
				form.find('[id^=reasons_list_]').prop('checked', false);

				for (var key in json.lead.reasons_list) {
					form.find('#reasons_list_' + key).prop('checked', true);
				}
			}

			write_multiselect();

			title = 'Редактировать лид';
		}

		form.closest('.js-zvonko-accordion__body').siblings('.js-zvonko-accordion__bar').html(title);
	})
	setTimeout(function () {
		form.find('.js-zvonko-rti__input').trigger('zvonkoRefreshRTI');
	}, 1500);

}

function string_gen(len) {

	var text = '';
	var charset = 'abcdefghijklmnopqrstuvwxyz';

	for (var i = 0; i < len; i++) {
		text += charset.charAt(Math.floor(Math.random() * charset.length));
	}

	return text;
}

function init_lead_select() {

	$('.dropdown dt a').off('click.select_dropdown').on('click.select_dropdown', function () {
		$('.dropdown dd ul').slideToggle('fast');
	});

	$('.dropdown dd ul li a').off('click.select_li').on('click.select_li', function () {
		$('.dropdown dd ul').hide();
	});

	function getSelectedValue(id) {
		return $('#' + id).find('dt a span.value').html();
	}

	$(document).unbind('click.dropdown').bind('click.dropdown', function (e) {
		var $clicked = $(e.target);
		//if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
	});

	$('.mutliSelect input[type="checkbox"]').off('click.checkbox').on('click.checkbox', function () {
		write_multiselect()
	});

	$('.other_reason').off('click.other_reason').on('click.other_reason', function () {

		var checked = $(this).prop('checked');
		if (checked) {
			$('.dropdown dd ul').slideToggle('fast');
			$('textarea[name=other_reason]').closest('label').show('fast');
		} else {
			$('textarea[name=other_reason]').closest('label').hide('fast');
		}
	})

	$('input.target-item').off('click.target').on('click.target', function () {
		if ($(this).hasClass('no-target')) {
			$(this).closest('form').find('input[name=name]').closest('label').hide('fast');
			$(this).closest('form').find('textarea[name=comment]').closest('label').hide('fast');
			$(this).closest('form').find('#reasons_call_list_block').show('fast');
			$(this).closest('form').find('.dropdown dd ul').show('fast');
		} else {
			$(this).closest('form').find('input[name=name]').closest('label').show('fast');
			$(this).closest('form').find('textarea[name=comment]').closest('label').show('fast');
			$(this).closest('form').find('#reasons_call_list_block').hide('fast');
		}
	})
}

function write_multiselect() {
	var input_list = $('.mutliSelect input[type="checkbox"]');
	var html = '';
	var title = '';

	for (var i = 0; i < input_list.length; i++) {
		if ($(input_list[i]).is(':checked')) {
			title = $('.mutliSelect label[for=reasons_list_' + $(input_list[i]).val() + ']').html() + ',';
			html += '<span title="' + title + '">' + title + '</span>';
		}
	}

	$('.multiSel').html(html);

	if ($('.mutliSelect input[type="checkbox"]:checked').length == 0) {
		$('.hida').show();
	} else {
		$('.hida').hide();
	}
}

function show_block() {

	if ($('#urik').prop('checked')) {
		$('.hidden_urik').slideDown();
	} else {
		$('.hidden_urik').slideUp();
	}

}
