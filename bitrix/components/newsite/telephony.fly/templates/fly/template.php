<?
	$template_path = $this->GetFolder();
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . $template_path . '/css/telephony.css')) {
		Bitrix\Main\Page\Asset::getInstance()->addCss($template_path . '/css/telephony.css');
	}
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . $template_path . '/js/telephony.js')) {
		Bitrix\Main\Page\Asset::getInstance()->addJs($template_path . '/js/telephony.js');
	}
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . $template_path . '/js/action.js')) {
		Bitrix\Main\Page\Asset::getInstance()->addJs($template_path . '/js/action.js');
	}
?>

<script>
	var url_card_show = '<?=$template_path . '/ajax/card_show.php';?>';
	var url_get_statistic = '<?=$template_path . '/ajax/get_contact_deal_statistic.php';?>';
	var url_get_user_data = '<?=$template_path . '/ajax/get_user_data.php';?>';
	var url_action_lead = '<?=$template_path . '/ajax/action_lead.php';?>';
	var manager_id = <?=$USER->GetID()?>;
	var remote_ip = '<?=$_SERVER['REMOTE_ADDR'];?>';
	var ar_params = <?=json_encode($arParams);?>;
</script>

<div class="zvonko-overlay js-zvonko-overlay"></div>

<div class="zvonko js-zvonko">
	<div class="zvonko-fix">
		<div class="zvonko-main-btn js-zvonko-btn">
			<svg version="1.1"
				xmlns="http://www.w3.org/2000/svg"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				x="0px"
				y="0px"
				viewBox="0 0 1000 1000"
				enable-background="new 0 0 1000 1000"
				xml:space="preserve">
                <metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon</metadata>
				<g>
					<path d="M500.1,596.1c-78.3,0-151.9-30.5-207.2-85.8C237.5,454.9,207,381.4,207,303.1c0-78.3,30.5-151.9,85.8-207.2C348.2,40.5,421.8,10,500.1,10c78.3,0,151.9,30.5,207.2,85.8c55.4,55.4,85.8,128.9,85.8,207.2c0,78.3-30.5,151.9-85.8,207.2C651.9,565.7,578.3,596.1,500.1,596.1z M500.1,75.3c-125.6,0-227.7,102.2-227.7,227.7c0,125.6,102.2,227.7,227.7,227.7c125.6,0,227.7-102.2,227.7-227.7C727.8,177.5,625.6,75.3,500.1,75.3z"/>
					<path d="M891.4,957.1c0,18.2,14.7,32.9,32.9,32.9c18.2,0,32.9-14.7,32.9-32.9s-14.7-32.9-32.9-32.9C906.2,924.1,891.4,938.9,891.4,957.1z"/>
					<path d="M42.6,956.9c0,18.3,14.8,33.1,33.1,33.1c18.3,0,33.1-14.8,33.1-33.1s-14.8-33.1-33.1-33.1C57.4,923.9,42.6,938.7,42.6,956.9z"/>
					<path d="M500.1,597.2c205,0,374.7,158.3,391.3,359.1h66.1c-3.5-50.4-15.2-99.4-34.9-146c-23.1-54.6-56.2-103.6-98.2-145.7c-42.1-42.1-91.1-75.2-145.7-98.3c-56.6-23.9-116.6-36-178.5-36c-61.9,0-121.9,12.1-178.5,36c-54.6,23.1-103.6,56.2-145.7,98.3c-42.1,42.1-75.2,91.1-98.3,145.7c-19.7,46.6-31.4,95.6-34.9,146h66.1C125.3,755.5,295.1,597.2,500.1,597.2z"/>
				</g>
            </svg>
		</div>
		<span class="zvonko-main-btn-shadow"></span>
	</div>

	<div class="zvonko-widget js-zvonko-widget">
		<div class="zvonko-widget__box">
			<div class="zvonko-widget__container">
				<div class="zvonko-widget__container-inner">
					<h4 id="telephony_title"></h4>
					<div class="zvonko-close js-zvonko-close"></div>

					<div class="zvonko-accordion js-zvonko-accordion">

						<div class="zvonko-accordion__item js-zvonko-accordion__item" data-telephony-part="statistic">
							<div class="zvonko-accordion__bar js-zvonko-accordion__bar">Сделки</div>
							<div class="zvonko-accordion__body js-zvonko-accordion__body">
								<div class="zvonko-form-row js-zvonko-form-row">
									<ul class="statistic-list"></ul>
								</div>
							</div>
						</div>

						<div class="zvonko-accordion__item js-zvonko-accordion__item" data-telephony-part="interests">
							<div class="zvonko-accordion__bar js-zvonko-accordion__bar">Интересы</div>
							<div class="zvonko-accordion__body js-zvonko-accordion__body">

								<form action="<?= $template_path ?>/ajax/card_show.php"
									id="telephony_interests_form"
									class="ajax-send-form zvonko-form zvonko-form--call js-zvonko-form js-zvonko-form--call"></form>

							</div>
						</div>

						<div class="zvonko-accordion__item js-zvonko-accordion__item" data-telephony-part="notes">
							<div class="zvonko-accordion__bar js-zvonko-accordion__bar">Заметки</div>
							<div class="zvonko-accordion__body js-zvonko-accordion__body">

								<form action="<?= $template_path ?>/ajax/card_show.php"
									id="telephony_notes_form"
									class="ajax-send-form zvonko-form zvonko-form--call js-zvonko-form js-zvonko-form--call"></form>

							</div>
						</div>

						<div class="zvonko-accordion__item js-zvonko-accordion__item open" data-telephony-part="auth_form">
							<div class="zvonko-accordion__bar js-zvonko-accordion__bar">Авторизация</div>
							<div class="zvonko-accordion__body js-zvonko-accordion__body">
								<div id="seweral_comp"></div>
								<div class="zvonko-form-row">
									<select id="select_crm" name="crm_version">
										<option value="bitrix">Битрикс</option>
										<option value="magento">Magento</option>
									</select>
								</div>
								<div class="zvonko-form-row" id="select_input"></div>
								<form class="zvonko-form zvonko-form--msg js-zvonko-form js-zvonko-form--msg" id="telephony_auth_form">

									<div class="zvonko-form__inputs">
										<div class="zvonko-form-row js-zvonko-form-row user-name-container" style="display: none">
											<label class="zvonko-rti js-zvonko-rti">
												<input type="text"
													readonly
													class="zvonko-rti__input zvonko-input js-zvonko-rti__input"
													name="user_name">
												<span class="zvonko-rti__label js-zvonko-paste-error-here">Имя пользователя БУС</span>
												<span class="clear-button-input"></span>
											</label>
											<!--div class="zvonko-form-error-msg js-zvonko-paste-error-here"></div-->
										</div>
										<div class="zvonko-form-row js-zvonko-form-row" style="display: none">
											<label class="zvonko-rti js-zvonko-rti">
												<input type="text"
													class="zvonko-rti__input zvonko-input js-zvonko-rti__input"
													value="re-store.by"
													name="auth_link">
												<span class="zvonko-rti__label js-zvonko-paste-error-here">адрес сайта</span>
											</label>
											<!--div class="zvonko-form-error-msg js-zvonko-paste-error-here"></div-->
										</div>
										<?
											if (!empty($arResult['FILDS_UF_SEARCH'])):?>
												<? foreach ($arResult['FILDS_UF_SEARCH'] as $fields):
													if (!empty($fields['CODE'])):?>

														<div class="zvonko-form-row js-zvonko-form-row">
															<label class="zvonko-rti js-zvonko-rti">
																<input type="text"
																	class="zvonko-rti__input zvonko-input js-zvonko-rti__input "
																	name="<?= $fields['CODE']; ?>">
																<span class="zvonko-rti__label js-zvonko-paste-error-here"><?= $fields['NAME']; ?></span>
															</label>
															<!--div class="zvonko-form-error-msg js-zvonko-paste-error-here"></div-->
														</div>
													<? endif; ?>
												<? endforeach; ?>
											<? endif; ?>
										<div class="zvonko-form-row js-zvonko-form-row">
											<label class="zvonko-rti js-zvonko-rti">
												<input type="text"
													class="zvonko-rti__input zvonko-input js-zvonko-rti__input number-only"
													name="phone">
												<span class="zvonko-rti__label js-zvonko-paste-error-here">номер телефона</span>
											</label>
											<!--div class="zvonko-form-error-msg js-zvonko-paste-error-here"></div-->
										</div>
										<div class="zvonko-form-row js-zvonko-form-row">
											<label class="zvonko-rti js-zvonko-rti">
												<input type="text"
													class="zvonko-rti__input zvonko-input js-zvonko-rti__input"
													name="email">
												<span class="zvonko-rti__label js-zvonko-paste-error-here">Email</span>
											</label>
											<!--div class="zvonko-form-error-msg js-zvonko-paste-error-here"></div-->
										</div>
										<div class="zvonko-form-row js-zvonko-form-row">
											<label class="zvonko-rti js-zvonko-rti">
												<input type="text" class="zvonko-rti__input zvonko-input js-zvonko-rti__input number-only"
													name="user_id" maxlength="18">
												<span class="zvonko-rti__label js-zvonko-paste-error-here">id пользователя</span>
											</label>
											<!--div class="zvonko-form-error-msg js-zvonko-paste-error-here"></div-->
										</div>

										<div class="zvonko-form-row zvonko-form-row--offset">
											<button type="submit" name="mode" value="get" class="zvonko-btn zvonko-btn--primary">Искать в
												БУС
											</button>
											<button type="submit"
												name="mode"
												value="auth"
												style="background:#1058d0; color:#fff; border:1px solid #1058d0;"
												class="zvonko-btn zvonko-btn--primary">Авторизоваться
											</button>
										</div>
									</div>

								</form>
							</div>
						</div>

						<div class="zvonko-accordion__item js-zvonko-accordion__item" data-telephony-part="register_form">
							<div class="zvonko-accordion__bar js-zvonko-accordion__bar"></div>

							<div class="zvonko-accordion__body js-zvonko-accordion__body">
								<p style="color:red;">Контрагент не найден. Попробуйте изменить параметры поиска</p>
								<!--<div class="zvonko-form-error-msg js-zvonko-paste-error-here form-error-response"></div>
                                <form class="zvonko-form zvonko-form--msg js-zvonko-form js-zvonko-form--msg" action="<?= $template_path ?>/ajax/register_user_bus.php" id="telephony_register_form">

                                    <div class="zvonko-form__inputs">
                                        <div class="zvonko-form-row js-zvonko-form-row user-name-container">
                                            <label class="zvonko-rti js-zvonko-rti">
                                                <input type="text" class="zvonko-rti__input zvonko-input js-zvonko-rti__input" name="user_name">
                                                <span class="zvonko-rti__label js-zvonko-paste-error-here">Имя пользователя</span>
                                            </label>
                                            <div class="zvonko-form-error-msg js-zvonko-paste-error-here">Введите имя пользователя</div>
                                        </div>
                                        <div class="zvonko-form-row js-zvonko-form-row">
                                            <label class="zvonko-rti js-zvonko-rti">
                                                <input type="text" class="zvonko-rti__input zvonko-input js-zvonko-rti__input number-only" name="phone">
                                                <span class="zvonko-rti__label js-zvonko-paste-error-here">номер телефона</span>
                                            </label>
                                            <div class="zvonko-form-error-msg js-zvonko-paste-error-here">Введите номер телефона</div>
                                        </div>
										 <div class="zvonko-form-row js-zvonko-form-row">
                                            <label class="zvonko-rti js-zvonko-rti">
                                                <input type="checkbox" class="zvonko-uri__input" value="1" name="urik" id="urik" onclick="show_block();">
                                                <span class="zvonko-rti__label js-zvonko-paste-error-here urik">Регестрация Юр. лица</span>
                                            </label>
                    
                                        </div>
										<div class="hidden_urik">
									     <div class="zvonko-form-row js-zvonko-form-row">
                                            <label class="zvonko-rti js-zvonko-rti">
                                                <input type="text" class="zvonko-rti__input zvonko-input js-zvonko-rti__input number-only" name="company_name">
                                                <span class="zvonko-rti__label js-zvonko-paste-error-here">название компании</span>
                                            </label>
                                            <div class="zvonko-form-error-msg js-zvonko-paste-error-here">Введите название компании</div>
                                        </div>
										<div class="zvonko-form-row js-zvonko-form-row">
                                            <label class="zvonko-rti js-zvonko-rti">
                                                <input type="text" class="zvonko-rti__input zvonko-input js-zvonko-rti__input number-only" name="type_restaraunt">
                                                <span class="zvonko-rti__label js-zvonko-paste-error-here">тип заведения</span>
                                            </label>
                                            <div class="zvonko-form-error-msg js-zvonko-paste-error-here">Введите тип заведения</div>
                                        </div>
										<div class="zvonko-form-row js-zvonko-form-row">
                                            <label class="zvonko-rti js-zvonko-rti">
                                                <input type="text" class="zvonko-rti__input zvonko-input js-zvonko-rti__input number-only" name="email">
                                                <span class="zvonko-rti__label js-zvonko-paste-error-here">электронная почта</span>
                                            </label>
                                            <div class="zvonko-form-error-msg js-zvonko-paste-error-here">Введите электронная почта</div>
                                        </div>
										<div class="zvonko-form-row js-zvonko-form-row">
                                            <label class="zvonko-rti js-zvonko-rti">
                                                <input type="text" class="zvonko-rti__input zvonko-input js-zvonko-rti__input number-only" name="sity">
                                                <span class="zvonko-rti__label js-zvonko-paste-error-here">город</span>
                                            </label>
                                            <div class="zvonko-form-error-msg js-zvonko-paste-error-here">Введите город</div>
                                        </div>
										</div>
                                        <div class="zvonko-form-row zvonko-form-row--offset">
                                            <button type="submit" name="mode" value="register" class="zvonko-btn zvonko-btn--primary">Регистрация юзера и контакта</button>
                                        </div>
                                        <div class="zvonko-form-msg zvonko-form-msg--default contact-conversion-message">При конвертации лида выберите контакт <strong></strong></div>
                                    </div>

                                </form>
                            </div>-->
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
