<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

\CModule::IncludeModule('crm');

$ar_group_list = [];
$db_group = CGroup::GetList( $by = 'SORT', $order = 'ASC', []);
while ($ar_group = $db_group->Fetch()){
    $ar_group_list[$ar_group['ID']] = $ar_group['NAME'];
}

$db_multy = \CAllUserTypeEntity::GetList(
    ['ID' => 'ASC'],
    ['ENTITY_ID' => 'CRM_LEAD','MULTIPLE' => 'Y', 'LANG' => 'ru']
);
$ar_user_fields_multy = [];
while ($ar_user_field = $db_multy->Fetch()){
    $ar_user_fields_multy[$ar_user_field['ID']] = !empty($ar_user_field['EDIT_FORM_LABEL']) ? $ar_user_field['EDIT_FORM_LABEL'] : $ar_user_field['FIELD_NAME'];
}

$db_user_type = \CAllUserTypeEntity::GetList(
    ['ID' => 'ASC'],
    ['ENTITY_ID' => 'CRM_LEAD','MULTIPLE' => 'N', 'LANG' => 'ru']
);
$ar_user_fields_no_multy = [];
while ($ar_user_field = $db_user_type->Fetch()){
    $ar_user_fields_no_multy[$ar_user_field['ID']] = !empty($ar_user_field['EDIT_FORM_LABEL']) ? $ar_user_field['EDIT_FORM_LABEL'] : $ar_user_field['FIELD_NAME'];
}

$db_user_type = \CAllUserTypeEntity::GetList(
    ['ID' => 'ASC'],
    ['ENTITY_ID' => 'CRM_LEAD','USER_TYPE_ID' => 'string', 'LANG' => 'ru']
);
$ar_user_fields_string = [];
while ($ar_user_field = $db_user_type->Fetch()){
    $ar_user_fields_string[$ar_user_field['ID']] = !empty($ar_user_field['EDIT_FORM_LABEL']) ? $ar_user_field['EDIT_FORM_LABEL'] : $ar_user_field['FIELD_NAME'];
}


$arComponentParameters = array(
    "GROUPS" => [],
    "PARAMETERS" => array(
        "GROUP_LIST" => array(
            "PARENT" => "BASE",
            "NAME" => 'Группы, которым показывать компонент телефонии',
            "TYPE" => "LIST",
            "VALUES" => $ar_group_list,
            "DEFAULT" => "",
            "MULTIPLE" => "Y",
        ),
        "GROUP_LIST_BACKEND" => array(
            "PARENT" => "BASE",
            "NAME" => 'измените список групп в файле local/include/classes/telephony/telephony_class.php',
            "TYPE" => "TEXT",
            "DEFAULT" => "измените список групп в файле local/include/classes/telephony/telephony_class.php"
        ),
        
		"FILDS_UF_SEARCH" => array(
            "PARENT" => "BASE",
			'ADDITIONAL_VALUES' => 'Y',
			'MULTIPLE' => 'Y',
			"NAME" => GetMessage("NEWSITE_SEARCH_INPUT"),
            "TYPE" => "STRING",
            "DEFAULT" => ""
        )
    )
);
