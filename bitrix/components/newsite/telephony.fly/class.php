<?
class CTelephonyFly extends CBitrixComponent{

    function init_params(){
        $this->arResult['REASONS_CALL_LIST'] = [];
		$this->arResult['FILDS_UF_SEARCH'] = [];
        if(!empty($this->arParams['REASONS_CALL_FIELD'])){
            $db_reason_list = \CUserFieldEnum::GetList(['SORT' => 'ASC'],['USER_FIELD_ID' => (int)$this->arParams['REASONS_CALL_FIELD'] ]);
            while ($ar_val = $db_reason_list->Fetch()){
                $this->arResult['REASONS_CALL_LIST'][] = $ar_val;
            }
		}

			if(!empty($this->arParams['FILDS_UF_SEARCH'])){
          
            foreach($this->arParams['FILDS_UF_SEARCH'] as $param){
				
				$param = explode(':',$param);
                $this->arResult['FILDS_UF_SEARCH'][] = array('NAME'=>$param[0],'CODE'=>$param[1]);
            }
			
			
        }

        $this->arResult['TARGET_LIST'] = [];
        if(!empty($this->arParams['TARGET_FIELD'])){
            $db_reason_list = \CUserFieldEnum::GetList(['SORT' => 'ASC'],['USER_FIELD_ID' => (int)$this->arParams['TARGET_FIELD'] ]);
            while ($ar_val = $db_reason_list->Fetch()){
                $this->arResult['TARGET_LIST'][] = $ar_val;
                //echo '<pre>'.print_r($ar_val,true).'</pre>';
            }
        }
    }

    function executeComponent(){

        global $USER, $APPLICATION;
        $this->init_params();
        if($USER->IsAuthorized()){
            $ar_user_groups = (array)CUser::GetUserGroup($USER->GetID());
            $result = array_intersect($ar_user_groups, (array)$this->arParams['GROUP_LIST']);
            if(!empty($result)){
                $APPLICATION->AddHeadScript('/local/include/js/custom.js', true);
                $APPLICATION->AddHeadScript('/local/include/js/jquery-3.2.1.min.js', true);
                $this->includeComponentTemplate();
            }
        }
    }
}