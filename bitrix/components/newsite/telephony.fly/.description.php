<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("NEWSITE_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("NEWSITE_COMPONENT_DESCR"),
	"PATH" => array(
		"ID" => "newsite",
		"NAME" => GetMessage("NEWSITE_TOOLS")
	),
);?>